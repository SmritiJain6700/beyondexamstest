import os
import time
import json
import sys
from undetected_chromedriver import Chrome, ChromeOptions
from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from data.common_data import cwd
from pages.login_page import LoginPage


with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])


def wait_for_pageload(driver):
    try:
        WebDriverWait(driver, 100).until(
            lambda driver: driver.execute_script('return document.readyState') == 'complete')
        return True
    except TimeoutException:
        return False

def create_driver():
    options = webdriver.ChromeOptions()
    options.add_argument("user-data-dir={}/profile".format(cwd))
    if not os.path.exists("{}/profile".format(cwd)):
        os.makedirs("{}/profile".format(cwd))
    service = Service(executable_path=f"{cwd}/chromedriver.exe")
    driver = webdriver.Chrome(service=service, options=options)
    driver.maximize_window()
    return driver

def login(driver):
    login_page = LoginPage(driver)

    # Log in once and reuse the same session for each test method
    login_page.click_signup_button()
    WebDriverWait(driver, 100).until(EC.number_of_windows_to_be(2))
    driver.switch_to.window(driver.window_handles[1])
    login_page.enter_email(settings['testLoginData']['email'])
    login_page.click_next_button()
    login_page.enter_password(settings['testLoginData']['password'])
    login_page.click_login_button()
    driver.switch_to.window(driver.window_handles[0])
    # Wait for the page to complete the login process
    element = WebDriverWait(driver, 50).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, ".SelectorLoginBTN.btn-selector-p-2"))
    )
    if(element.text == 'Student'):
        return True
    else:
        return False
        