import unittest
import logging
import time
import json
import sys

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url
from helpers import wait_for_pageload, create_driver, login

# Configure logging to write to a file
logging.basicConfig(filename='error_for_talk_to_us_page.log', level=logging.ERROR)

class TalkToUsPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    def test_talk_to_us_back_to_course_button(self):
        print("\nStarted testing talk to us - Back to course button")
    
        try:
            
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Get the initial URL
            initial_url = self.driver.current_url
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()

            # Click the "Back to Course" button
            back_to_course_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[1]/div[1]/div[2]/a/img')))
            back_to_course_button.click()
            
            # Get the current URL after clicking the "Back to Course" button
            current_url = self.driver.current_url
            print(current_url, initial_url)
            
            # Check if the current URL is the same as the initial URL
            assert current_url == initial_url, "Failed to return to the initial URL."

            print("Successfully tested the back to course button")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Talk to us back to course_button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            

    def test_talk_to_us_notification_bell(self):
        print("\nStarted testing talk to us notification bell")
        
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()

            notification_bell = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[1]/div[1]/div[2]/img')))
            
            notification_bell.click()
            
            # Handle the popup
            popup = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))

            # Assert if the popup is displayed
            assert popup.is_displayed(), "Notification popup is not displayed"
        
            print("Successfully tested talk to us notification bell")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Talk to us notification bell test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

        
    def test_talk_to_us_button_send_a_message(self):
        #for this course the notifications are already enabled
        print("\nStarted testing talk to us button - send a message")
        
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()

            # Find the input field by its ID
            input_field = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="send-message-text"]')))

            self.driver.execute_script("arguments[0].scrollIntoView();", input_field)

            # Enter the desired text into the input field
            text_to_enter = "This is a new test"
            input_field.send_keys(text_to_enter)
            
            # Find the send button by its class name and SVG tag name
            send_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".pointer svg")))

            # Click the send button
            send_button.click()

            # Wait for the sent message to be displayed in the message box
            sent_message = WebDriverWait(self.driver, 50).until(EC.text_to_be_present_in_element((By.CLASS_NAME, "chat-box-container-left"), text_to_enter))

            # Assertion to check if the sent message is displayed
            assert sent_message, "Sent message not displayed in the message box"

            # If the sent message is displayed, the test passes
            print("Sent message is displayed in the message box.")

            print("Successfully tested the talk to us button - send a message")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Talk to us button-send a message test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
        
    def test_collapse_conversations_button(self):
        print("\nStarted testing collapse conversations button")
        
        try:
        
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()
            
            # Find the collapse icon element
            collapse_icon = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[1]/div/div/p/img'))
            )
            
            # Click the collapse icon
            collapse_icon.click()
            
            # Wait for the conversations to collapse
            conversation_container = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "all-chats-collapsed"))
            )

            # Verify if the conversations are collapsed/hidden
            assert conversation_container.is_displayed(), "Conversations are not collapsed after clicking the collapse button."

            print("Successfully tested the collapse conversations button")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Collapse conversations button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_attachment_options(self):
        print("\nStarted testing the attachment options")
        
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()
            
            # Find the attachment button
            attachment_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[2]/div/div/div[1]/img'))
            )

            # Check if the button is displayed and enabled
            self.assertTrue(attachment_button.is_displayed(), "Attachment button is not displayed.")
            self.assertTrue(attachment_button.is_enabled(), "Attachment button is not enabled.")
                
            # Click the attachment button
            attachment_button.click()

            # Wait for all div options in attachment to appear
            attachment_options = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[2]/div/div/div[2]/div'))
            )

            expected_attachment_options = ['Image', 'Video', 'Audio', 'Document']
            
            # Get the actual attachment options
            actual_attachment_options = []
            for option in attachment_options:
                span_element = option.find_element(By.XPATH, './/span')
                text = span_element.get_attribute('textContent')
                actual_attachment_options.append(text)

            # Compare and assert the expected and actual options
            assert expected_attachment_options == actual_attachment_options
            
            print("Successfully tested the attachment options")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Attachment options test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_help_button_watch_tutorial_options(self):
        print("\nStarted testing the help button - Watch Tutorial options")

        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
            
            # Click the "talk to us" button
            talk_to_us_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/button')))
            talk_to_us_button.click()
            
            # Find the element using XPath
            help_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()
            
            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Find the options within Watch Tutorial
            watch_tutorial_options = viewer_element.find_elements(By.CSS_SELECTOR, ".DesktopProfileBtn")
            
            # Get the text of each option
            watch_tutorial_options_texts = [option.text for option in watch_tutorial_options]
        
            expected_options = ["Refer and Earn", "Create your course"]

            # Print the text of each child element
            for option in expected_options:
                assert option in watch_tutorial_options_texts, f"Expected option '{option}' not found in the watch tutorial options."

            print("Successfully tested the watch tutorial button")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button watch tutorial options test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
        
        
if __name__ == '__main__':
    unittest.main()
        