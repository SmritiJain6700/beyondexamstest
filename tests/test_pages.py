import sys
import json
import unittest
import HtmlTestRunner

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from tests.test_login import LoginTest
from tests.test_home_page import HomePageTest
from tests.test_courses_page import CoursesPageTest
from tests.test_a_course_student_page_unenrolled import CourseUnEnrolledStudentPageTest
from tests.test_a_course_student_page_enrolled import CourseEnrolledStudentPageTest
from tests.test_all_common_elements import CommonElementsTest
from tests.test_a_course_teacher_page import CousrseTeacherPageTest
from tests.test_video_page_teacher import VideoPageTeacherTest
from tests.test_video_page_student import VideoPageStudentTest
from tests.test_for_logged_out_users import LoggedOutPageTest
from tests.test_talk_to_us_page import TalkToUsPageTest
from selenium import webdriver
from helpers import create_driver, login


if __name__ == '__main__':
    test_suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    loaded_login_tests = loader.loadTestsFromTestCase(LoginTest)
    test_suite.addTests(loader.loadTestsFromTestCase(LoginTest))
    test_suite.addTests(loader.loadTestsFromTestCase(HomePageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(CoursesPageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(TalkToUsPageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(CommonElementsTest))
    test_suite.addTests(loader.loadTestsFromTestCase(CourseEnrolledStudentPageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(CourseUnEnrolledStudentPageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(VideoPageTeacherTest))
    test_suite.addTests(loader.loadTestsFromTestCase(VideoPageStudentTest))
    test_suite.addTests(loader.loadTestsFromTestCase(CousrseTeacherPageTest))
    test_suite.addTests(loader.loadTestsFromTestCase(LoggedOutPageTest))

    # Specify UTF-8 encoding for HTMLTestRunner
    HtmlTestRunner.HTMLTestRunner.OUTPUT_ENCODE_DEFAULT = 'utf-8'

    if not settings["debug"]:
        # Run the test suite using HTMLTestRunner
        runner = HtmlTestRunner.HTMLTestRunner(output='test_results')
    else:
        # Run the tests without generating a report
        runner = unittest.TextTestRunner()
    runner.run(test_suite)
