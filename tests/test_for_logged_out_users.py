import unittest
import logging
import time
import json
import sys
import traceback

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from pages.course_page import CoursePage
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url
from helpers import wait_for_pageload, create_driver, login
from data.test_data import valid_coupon_code, invalid_coupon_code

# Configure logging to write to a file
logging.basicConfig(filename='error_logged_out_users_page.log',level=logging.ERROR)

class LoggedOutPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)
        self.logout()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @classmethod
    def logout(self):
        # Find the profile button element
        profile_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div')))

        # Perform a mouseover action using JavaScript to trigger the event
        script = "var event = new MouseEvent('mouseover', { 'view': window, 'bubbles': true, 'cancelable': true });" \
                "arguments[0].dispatchEvent(event);"
        self.driver.execute_script(script, profile_button)

        # Locate the 'logout' button
        logout_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/div[10]/div[2]')))

        # Click the go to your dashboard button using JavaScript
        self.driver.execute_script("arguments[0].click();", logout_button)

        confirmation_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

        # Find and click the 'Ok button' in warming popup
        ok_button = WebDriverWait(confirmation_popup, 20).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
        )
        
        self.driver.execute_script("arguments[0].click();", ok_button)

        time.sleep(5)

    def test_enroll_now_button_for_logged_out_user(self):
        print("\nStarted testing enroll now button for logged out user")
        
        try:
            # Navigate to an unpaid course
            self.course_page.navigate_to_a_paid_course()

            # Locate the enroll now button on the course page
            enroll_now_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[1]/button[1]')))
            
            # Click the enroll now button
            enroll_now_button.click()

            # Check if the button is displayed and enabled
            self.assertTrue(enroll_now_button.is_displayed(), "Enroll now button is not displayed.")
            self.assertTrue(enroll_now_button.is_enabled(), "Enroll now button is not enabled.")
            
            login_with_google_popup = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]')))

            try:
                login_with_google_popup = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]')))
                assert login_with_google_popup.is_displayed()
                print("Login popup is displayed.")
            except:
                print("Login popup is not displayed.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Enroll now button for a course test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

if __name__ == '__main__':
    
    unittest.main()