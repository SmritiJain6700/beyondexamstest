import unittest
import time
import json
import sys
import logging


with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from pages.home_page import HomePage
from helpers import wait_for_pageload, create_driver


# Configure logging to write to a file
logging.basicConfig(filename='error.log', level=logging.ERROR)

class HomePageTest(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.home_page = HomePage(self.driver)
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
    
    def setUp(self):
        # Set up instance-level preconditions for each test
        self.navigate_to_home_page()
            
    def navigate_to_home_page(self):
        #return to homepage
        self.home_page.load()

    def test_title(self):
        print("\nStarted testing the title of homepage")
                
        try:
            # Get the title of the page
            title = self.driver.title

            # Verify if the title matches the expected value
            expected_title = "BeyondExams"
            self.assertEqual(title, expected_title)
            
            print("Successfully tested the title of the homepage")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Title test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    def test_home_page_search_bar(self):
        print("\nStarted testing home search bar working properly")

        try:
            # Find the search input element and enter the search term 'computer'
            search_input = self.driver.find_element(By.CLASS_NAME, 'home-search-input')
            search_input.send_keys('computer')
            search_input.send_keys(Keys.RETURN)

            # Wait for the presence of search results
            results_displayed = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'search_mid'))
            ).is_displayed()
            self.assertTrue(results_displayed)

            print("Successfully tested the home search bar working properly.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Homepage search bar test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
                
    def test_courses_dropdown(self):
        print("\nStarted testing courses dropdown button.")
                
        courses_dropdown_xpath = '//*[@id="root"]/div[5]/div/div/div[1]/div[1]/div[1]/div/button'
        
        try:
            # Locate the courses dropdown element
            courses_dropdown = self.driver.find_element(By.XPATH, courses_dropdown_xpath)

            # Click on the courses dropdown to open it
            courses_dropdown.click()

            # Wait for the dropdown options to load
            dropdown_options = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="root"]/div[5]/div/div/div[1]/div[1]/div[1]/div/div/ul/li')))
            # Verify that the dropdown options contain "Topics" and "Classes"
            expected_options = ["Topics", "Classes"]

            # Get the text of each dropdown option
            dropdown_option_texts = [option.text for option in dropdown_options]
            
            # Verify the presence of expected options in the dropdown
            for option in expected_options:
                assert option in dropdown_option_texts, f"Expected option '{option}' not found in the dropdown options."
        
            print("Successfully tested Courses dropdown working.")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Courses dropdown test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_best_course_loaded_successfully(self):
        print("\nStarted testing if the best course loads successfully.")

        # Scroll the page to bring the element into view
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        
        course_button_xpath = '//*[@id="root"]/div[10]/div/div[2]/div/div[3]/div/div[1]/span/a'
        
        try:
            # Wait for the course button to be clickable
            course_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, course_button_xpath)))
            self.driver.execute_script("arguments[0].scrollIntoView();", course_button)
            
            # Click the course button
            course_button.click()

            # Wait for page to load successfully
            course_detail_card_xpath = '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div'
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, course_detail_card_xpath)))

            # Assert if course detail card is displayed
            course_detail_card = self.driver.find_element(By.XPATH, course_detail_card_xpath)
            self.assertTrue(course_detail_card.is_displayed())

            print("Successfully tested if best course loaded.")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Best Courses loading test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
        
if __name__ == '__main__':
    unittest.main()
