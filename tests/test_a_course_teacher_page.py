import unittest
import pyautogui
import logging
import time
import json
import sys
import random
import string

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from pages.course_page import CoursePage
from data.urls import learn_a_skill_url
from helpers import wait_for_pageload, create_driver, login
from data.test_data import test_file_path, test_youtube_url, test_course_url, testing_pdf, new_module_test_url, existing_module_url
from datetime import datetime, timedelta

# Configure logging to write to a file
logging.basicConfig(filename='error_for_a_course_teacher_page.log', level=logging.INFO)

class CousrseTeacherPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)

        # Check if the role of the user is "student"
        if self.course_page.get_user_role() == "S":
            # Switch the role to "student"
            self.course_page.switch_role()
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    @classmethod
    def setUp(self):
        time.sleep(2)
        
    def test_endorse_course_button(self):
        #The course is initially not endorsed
        print("\nStarted testing the endorse course button")
        
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Find the endorse course button
            endorse_course_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[1]/button[1]')))

            # Get the initial text of the endorse course button
            initial_text = endorse_course_button.text

            if(initial_text == 'ENDORSE'):

                # Click the endorse course button
                endorse_course_button.click()

                # Use WebDriverWait to wait until the element is visible
                wait = WebDriverWait(self.driver, 10)
                star_rating = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div[3]/div/div[2]/span/label[5]")))

                star_rating.click()

                # Use WebDriverWait to wait until the textarea is visible
                feedback_textarea = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div[3]/div/div[2]/textarea")))

                # Enter feedback
                feedback = "This course was excellent! I learned a lot."
                feedback_textarea.clear()
                feedback_textarea.send_keys(feedback)

                # Find the submit button
                submit_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[3]/div/div[2]/button")))

                # Submit the rating form
                submit_button.click()

                # Assert the success message
                success_message = wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))
                expected_text = "Rating added successfully"
                actual_text = success_message.text
                assert expected_text == actual_text, f"Expected: {expected_text}, Actual: {actual_text}"

                # Assert the updated text of the endorse course button
                updated_text = endorse_course_button.text
                expected_updated_text = "ENDORSED"
                assert expected_updated_text == updated_text, f"Expected: {expected_updated_text}, Actual: {updated_text}"

            else:
                print("Course is already endorsed")

            print("Successfully tested the endorse a course button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Endorse course button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
                
    def test_new_course_a_create_skill_course(self):
        print("\nStarted testing the create a new skill course feature for teachers")

        try:

            # Navigate to courses page to find the create a new course button
            self.driver.get(learn_a_skill_url)

            # Find the create a new course button
            create_new_course_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]')))

            # Click the create new course button
            create_new_course_button.click()

            # Start entering the details for the course creation
            
            # Wait for the name_of_course element to be located
            name_of_course = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[2]/input')))

            # Enter the desired text into the 'name_of_course' input field
            text_to_enter = "Test-Course-Web-Development"
            name_of_course.send_keys(text_to_enter)

            # Wait for the description_of_course element to be located
            description_of_course = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[3]/textarea')))

            # Enter the desired text into the description_of_course input field
            description_to_enter = "This is a test course for web development"
            description_of_course.send_keys(description_to_enter)

            # Wait for the next_button element to be clickable
            next_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[4]/button')))

            # Click the 'Next' button
            next_button.click()
                    
            # Locate the select domain dropdown button
            select_domain_dropdown_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div[2]/div[2]/div[1]/img')))
            
            # Click the dropdown button
            select_domain_dropdown_button.click()

            # Locate the dropdown option
            domain_dropdown_option = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/ul/li[1]')))

            # Click the dropdown option
            domain_dropdown_option.click()

            # Locate the select sub-domain dropdown button
            select_sub_domain_dropdown_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div[2]/div[2]/div[3]/img')))
            
            # Click the dropdown button
            select_sub_domain_dropdown_button.click()

            # Locate the dropdown option
            sub_domain_dropdown_option = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/ul/li[1]')))

            # Click the dropdown option
            sub_domain_dropdown_option.click()

            # Locate the select micro domain dropdown button
            select_micro_domain_dropdown_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div[2]/div[2]/div[5]/img')))
            
            # Click the dropdown button
            select_micro_domain_dropdown_button.click()

            # Locate the dropdown option
            micro_domain_dropdown_option = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/ul/li[2]')))

            # Click the dropdown option
            micro_domain_dropdown_option.click()
        
            # Wait for the next_button element to be clickable
            next_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div[3]/button[2]')))

            # Click the 'Next' button
            next_button.click()

            # Tell us more about the course you are creating.

            # Find the level of course button
            level_dropdown_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[3]/div')))
            
            # Click the dropdown button
            level_dropdown_button.click()

            # Locate the dropdown option
            level_dropdown_option = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="customized-menu"]/div[3]/ul/li[1]/div[2]/span')))

            # Click the dropdown option
            level_dropdown_option.click()

            # Find the keywords for course input field
            keywords = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[4]/label[2]/ul/input')))

            # Enter the desired text into the input field
            text_to_enter = "Web-Development"
            keywords.send_keys(text_to_enter)

            # Press the Enter key
            keywords.send_keys(Keys.RETURN)

            # Locate the file upload input element
            file_upload_input = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[5]/div/div/div/label/span')))
            
            # Scroll to the file upload input element
            self.driver.execute_script("arguments[0].scrollIntoView(true);", file_upload_input)

            # Click the file upload button
            file_upload_input.click()

            # Wait for the file selection dialog to appear
            pyautogui.sleep(1)

            # Set the file path to be uploaded
            file_path = path = test_file_path

            # Type the file path in the file selection dialog
            pyautogui.write(file_path)

            # Press Enter to confirm the file selection
            pyautogui.press("enter")

            # Locate the cropper-face element
            cropper_box = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,'/html/body/div[3]/div[3]/div[1]/div/div[3]')))

            # Perform a drag and drop action to move the cropper box
            actions = ActionChains(self.driver)
            actions.click_and_hold(cropper_box).move_by_offset(10, 10).release().perform()
            
            # Find the done button 
            done_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div[3]/div[2]/button[1]')))

            # Click the done option
            done_button.click()

            # Wait for the next_button element to be clickable
            next_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div/div[6]/button[2]')))

            # Click the 'Next' button
            next_button.click()

            #Locate the add content button
            add_content_video = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn')))

            #Click the add content button
            add_content_video.click()

            # Select the type of content you want to add
            content_dropdown = self.driver.find_element(By.CLASS_NAME, 'drop-list')
            content_dropdown_options = content_dropdown.find_elements(By.CLASS_NAME, 'MuiListItem-button')
            content_dropdown_options[0].click()
        
            # Enter the url into video field
            video_input_field = self.driver.find_element(By.CLASS_NAME, 'add-video-input')
            text_to_enter = test_youtube_url
            video_input_field.clear()
            video_input_field.send_keys(text_to_enter)

            # Find the fetch button
            fetch_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'videoFetchBtn')))

            fetch_button.click()

            # Locate and click the submit button
            submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/button')))

            submit_button.click()
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Video added successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"

            # Wait for the next_button element to be clickable
            next_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[2]/div[3]/button[2]')))

            self.driver.execute_script("arguments[0].scrollIntoView(true);", next_button)
            # Click the 'Next' button
            next_button.click()

            # Wait for turn on notifications for chat dialog to appear
            turn_on_notifications = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div')))

            # Find the "Yes" button 
            yes_button = turn_on_notifications.find_element(By.XPATH, '/html/body/div[2]/div/div[3]/div[2]')

            # Click the "Yes" button
            yes_button.click()

            print("Successfully tested the create a new skill course feature for teachers")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Create Skill course test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_new_course_b_edit_course_button(self):
        print("\nStarted testing the edit course button")

        try:
            # Perform the search to locate the desired course

            # Click on the search bar
            search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
            search_bar.click()

            # Find the search input element and enter the search term
            search_input = self.driver.find_element(By.XPATH, '//*[@id="nav-search"]')
            search_input.send_keys('Test-Course-Web-Development')
            search_input.send_keys(Keys.RETURN)

            # Click on the 'Courses' tab
            courses_tab = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[1]/div[2]/div[2]/div/button[2]'))
            )
            courses_tab.click()

            # Click on the desired course
            course_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[2]/div/div[1]/div/div/a'))
            )
            course_button.click()

            # Find and click the 'Edit Course' button

            # Define the CSS selector for the buttons
            button_selector = ".MuiButtonBase-root.MuiIconButton-root.CourseHeader_course_icon__2oOve"

            # Wait for the buttons to be present and visible
            course_header_buttons = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CSS_SELECTOR, button_selector))
            )

            # Locate the edit course button
            edit_course_button = course_header_buttons[2]

            # Click the edit course button
            edit_course_button.click()

            # Edit the course details

            # Click the dropdown button to select a domain
            select_domain_dropdown_button = WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'Dropdown_dropdown__2JICB'))
            )
            select_domain_dropdown_button.click()

            # Select the desired domain from the dropdown
            domain_dropdown_option = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[1]/div/ul/li[1]'))
            )
            actions = ActionChains(self.driver)
            actions.move_to_element(domain_dropdown_option).perform()
            sub_domain_dropdown_option = domain_dropdown_option.find_element(By.XPATH, './/ul/li[1]')
            actions.move_to_element(sub_domain_dropdown_option).perform()
            micro_domain_dropdown_option = sub_domain_dropdown_option.find_element(By.XPATH, './/ul/li[2]')
            micro_domain_dropdown_option.click()

            # Update the course name
            name_of_course = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[4]/div/input')
            name_of_course.click()
            name_of_course.send_keys(Keys.CONTROL + "a")  # Select all text in the input field
            name_of_course.send_keys(Keys.BACKSPACE)
            text_to_enter = "Test-course-new-Development"
            name_of_course.send_keys(text_to_enter)

            # Update the course description
            description_of_course = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[6]/div/textarea[1]')
            description_of_course.click()
            description_of_course.send_keys(Keys.CONTROL + "a")  # Select all text in the input field
            description_to_enter = "This is a test course for web development"
            description_of_course.send_keys(description_to_enter)

            # Select the course level
            level_dropdown_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[8]/div/div'))
            )
            level_dropdown_button.click()
            level_dropdown_option = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="menu-"]/div[3]/ul/li[1]'))
            )
            level_dropdown_option.click()

            #add keywords
            keywords = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[10]/div/textarea[1]')))
            keywords.click()
            keywords_to_enter = "Software Web development testing"
            keywords.send_keys(Keys.CONTROL + "a")
            keywords.send_keys(keywords_to_enter)

            #check course visibility dropdown
            course_visibility_dropdown  = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/div[3]/div/div')))
            course_visibility_dropdown.click()

            public_option = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="menu-"]/div[3]/ul/li[1]')))
            public_option.click()

            # Update course pricing (if applicable)
            course_type_dropdown = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/div[5]/div/div')))
            course_type_dropdown.click()

            paid_course_option = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="menu-"]/div[3]/ul/li[1]')))
            paid_course_option.click()
            is_checked = True

            if is_checked:
                price_input = self.driver.find_element(By.XPATH,'//*[@id="common-dialog-description"]/div/div[2]/div[7]/div/input')
                price = 100  # Example price value
                price_input.send_keys(Keys.CONTROL + "a")
                price_input.send_keys(str(price))

                discount_input = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/div[9]/div/input')
                discount = 20  # Example discount value
                discount_input.send_keys(Keys.CONTROL + "a")
                discount_input.send_keys(str(discount))

            # Click the update button
            update_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[3]/div[3]/div/div[3]/button'))
            )
        
            self.driver.execute_script("arguments[0].click();", update_button)

            # Wait for the success message to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-text'))
            )

            # Assert the success message text
            assert success_message.text == 'Course updated successfully'

            confirmation_popup = WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(confirmation_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            print("Successfully tested the edit course button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Edit course button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_course_c_name_and_description_of_course(self):
        print("\nStarted testing the name and description of a course")

        try:
            # Click on the search bar
            search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
            self.driver.execute_script("arguments[0].click();", search_bar)

            # Find the search input element and enter the search term
            search_input = self.driver.find_element(By.XPATH, '//*[@id="nav-search"]')
            search_input.send_keys('Test-course-new-Development')
            search_input.send_keys(Keys.RETURN)

            # Click on the 'Courses' tab
            courses_tab = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[1]/div[2]/div[2]/div/button[2]'))
            )
            courses_tab.click()

            # Click on the desired course
            course_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[2]/div/div[1]/div/div/a'))
            )
            course_button.click()

            # Check if name of the course is correct displayed
            # Locate the course name element
            course_name_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/h1'))
            )

            # Get the text content of the course name element
            course_name = course_name_element.text

            # Define the expected course name
            expected_course_name = 'Test-course-new-Development'

            # Assert the course name
            assert course_name == expected_course_name, f"Course name assertion failed. Expected: {expected_course_name}, Actual: {course_name}"

            # Wait for the description element to be located and visible
            description_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[1]'))
            )

            # Get the text content of the description element
            description = description_element.text

            # Define the expected description
            expected_description = 'This is a test course for web development'

            # Assert the description
            assert description == expected_description, f"Description assertion failed."

            print("Successfully tested the name and description of a course")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Name and description of course test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_course_d_breadcrumbs_of_course(self):
        print("\nStarted testing the breadcrumbs of a course")

        try:
            # Click on the search bar
            search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
            self.driver.execute_script("arguments[0].click();", search_bar)

            # Find the search input element and enter the search term
            search_input = self.driver.find_element(By.XPATH, '//*[@id="nav-search"]')
            search_input.send_keys('Test-course-new-Development')
            search_input.send_keys(Keys.RETURN)

            # Click on the 'Courses' tab
            courses_tab = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[1]/div[2]/div[2]/div/button[2]'))
            )
            courses_tab.click()

            # Click on the desired course
            course_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[2]/div/div[1]/div/div/a'))
            )
            course_button.click()

            # Wait for the breadcrumbs element to be located and visible
            breadcrumbs_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'crumbs'))
            )

            # Find all breadcrumb items within the breadcrumbs element
            breadcrumb_items = breadcrumbs_element.find_elements(By.CLASS_NAME, 'course-bread-crumb-item')

            # Extract the text content of each breadcrumb item
            breadcrumbs = [item.text.strip() for item in breadcrumb_items]

            # Join the breadcrumb items with the separator ' > '
            breadcrumbs_text = ' > '.join(breadcrumbs)

            # Define the expected breadcrumbs
            expected_breadcrumbs = 'Learn a skill > Coding > Web Development > JavaScript > Test-course-new-Development'

            # Assert the breadcrumbs
            assert breadcrumbs_text == expected_breadcrumbs, f"Breadcrumbs assertion failed. Expected: {expected_breadcrumbs}, Actual: {breadcrumbs_text}"

            print("Successfully tested the breadcrumbs of a course")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Breadcrumbs of course test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
                         
    def test_new_course_e_delete_course_button(self):
        print("\nStarted testing the delete course button")

        try:
        
            search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
            self.driver.execute_script("arguments[0].click();", search_bar)

            # Find the search input element and enter an invalid search term 'computer'
            search_input = self.driver.find_element(By.XPATH, '//*[@id="nav-search"]')
            search_input.send_keys('Test-course-new-Development')
            search_input.send_keys(Keys.RETURN)
            
            
            courses_tab = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[1]/div[2]/div[2]/div/button[2]')))

            # Click the 'Next' button
            courses_tab.click()

            course_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[2]/div/div[1]/div/div/a')))
            
            # Click the 'Next' button
            course_button.click()

            # Locate the delete course button
            delete_course_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[4]')))

            # Click the 'Next' button
            delete_course_button.click()

            # Wait for the delete course confirmation dialog to be visible
            delete_course_confirmation = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div')))

            # Find the "Yes" button within the delete_course_confirmation
            yes_button = delete_course_confirmation.find_element(By.XPATH, '/html/body/div[2]/div/div[4]/div[2]')

            # Click the "Yes" button
            yes_button.click()

            time.sleep(5)

            # Wait for the successful course removal dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[3]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "course is removed successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"

            print("Successfully tested the  delete course button")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Delete course button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

        def test_manage_coupon_button(self):
            print("\nStarted testing the working of manage coupon button")
            
            try:
                # Navigate to a paid course
                self.course_page.navigate_to_a_paid_course()

                # Check the presence of the "Manage Coupon" button
                manage_coupon_button = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[7]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
                )

                # Check if the "Manage Coupon" button is displayed
                self.assertTrue(manage_coupon_button.is_displayed(), "Manage Coupon button is not displayed.")

                # Check if the "Manage Coupon" button is enabled
                self.assertTrue(manage_coupon_button.is_enabled(), "Manage Coupon button is not enabled.")

                # Click the "Manage Coupon" button
                manage_coupon_button.click()

                # Use WebDriverWait to wait until the dialog is visible
                dialog = WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div[3]/div"))
                )

                # Check if the dialog is opened
                self.assertTrue(dialog.is_displayed(), "Manage Coupon dialog is not displayed.")

                #Check if Add New Coupon button is displayed in the manage coupon dialog
                add_new_coupon_button = dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/button/span[1]')
                self.assertTrue(add_new_coupon_button.is_displayed(), "Add New Coupon button is not displayed in the Manage Coupon dialog.")

                print("Successfully tested the working of manage coupon button")

            except Exception as e:
                # Handle exceptions that may occur during the test
                error_message = "Manage coupon button test failed."

                logging.error("An error occurred: %s", error_message)
                self.fail(error_message)

    
    def test_add_coupon_percentage_a_discount(self):
        print("\nStarted testing Add new coupon with percentage discount")
        
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Click "Manage Coupon" button
            manage_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
            )
            manage_coupon_button.click()

            # Click "Add new Coupon" button
            add_new_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/button/span[1]'))
            )
            add_new_coupon_button.click()

            # Check if the "Add New Coupon" button is displayed
            self.assertTrue(add_new_coupon_button.is_displayed(), "Add New button is not displayed.")

            # Check if the "Add New Coupon" button is enabled
            self.assertTrue(add_new_coupon_button.is_enabled(), "Add New Coupon button is not enabled.")

            # Find the input element for the discount code
            discount_code_input = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/form/div[1]/div/input'))
            )

            # Select the current value in the input field
            discount_code_input.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            discount_code_input.send_keys(Keys.BACKSPACE)

            # Enter a new discount code
            # Generate a random suffix
            suffix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))

            # Create the discount code with the "NEW-CODE-" prefix and the random suffix
            discount_code = "NEW-CODE-" + suffix
            discount_code_input.send_keys(discount_code)

            # Find the percentage discount button
            percentage_discount_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[2]/div/button[2]')

            # Click the percentage discount button
            percentage_discount_button.click()

            # Find the input element for the discount amount
            discount_amount_input = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[3]/div/input')

            # Select the current value in the input field
            discount_amount_input.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            discount_amount_input.send_keys(Keys.BACKSPACE)

            # Enter a new discount amount
            discount_amount = "30"
            discount_amount_input.send_keys(discount_amount)

            # Find the input element for the number of students
            number_of_students = self.driver.find_element(By.XPATH, '//*[@id="num-of-students"]')

            # Enter the number of students
            number_of_students.send_keys(5)

            # Find the submit button
            submit_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[2]')

            # Click the submit button
            submit_button.click()

            popup_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))

            popup_text = popup_element.text
            expected_text = "Coupon code created successfully"
            assert popup_text == expected_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}"

            print("Successfully tested the Add new coupon with percentage discount")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Add coupon percentage discount test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_add_coupon_b_flat_discount(self):
        print("\nStarted testing Add new coupon with flat discount")

        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Click "Manage Coupon" button
            manage_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
            )
            manage_coupon_button.click()

            # Click "Add new Coupon" button
            add_new_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/button/span[1]'))
            )
            add_new_coupon_button.click()

            # Check if the "Add New Coupon" button is displayed
            self.assertTrue(add_new_coupon_button.is_displayed(), "Add New button is not displayed.")

            # Check if the "Add New Coupon" button is enabled
            self.assertTrue(add_new_coupon_button.is_enabled(), "Add New Coupon button is not enabled.")

            # Find the input element for the discount code
            discount_code_input = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/form/div[1]/div/input'))
            )

            # Select the current value in the input field
            discount_code_input.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            discount_code_input.send_keys(Keys.BACKSPACE)

            # Enter a new discount code
            # Generate a random suffix
            suffix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))

            # Create the discount code with the "NEW-CODE-" prefix and the random suffix
            discount_code = "NEW-CODE-" + suffix
            discount_code_input.send_keys(discount_code)

            # Find the flat discount button
            flat_discount_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[2]/div/button[1]')

            # Click the flat discount button
            flat_discount_button.click()

            # Find the input element for the discount amount
            discount_amount_input = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[3]/div/input')

            # Select the current value in the input field
            discount_amount_input.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            discount_amount_input.send_keys(Keys.BACKSPACE)

            # Enter a new discount amount
            discount_amount = "30"
            discount_amount_input.send_keys(discount_amount)

            # Find the input element for the number of students
            number_of_students = self.driver.find_element(By.XPATH, '//*[@id="num-of-students"]')

            # Enter the number of students
            number_of_students.send_keys(5)

            # Find the submit button
            submit_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[2]')

            # Click the submit button
            submit_button.click()

            popup_element = WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))

            popup_text = popup_element.text
            expected_text = "Coupon code created successfully"
            assert popup_text == expected_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}"

            print("Successfully tested the Add new coupon with flat discount")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Add coupon flat discount test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_copy_coupon_button(self):
        print("\nStarted testing the coupon copy button")
        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Click "Manage Coupon" button
            manage_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
            )
            manage_coupon_button.click() 

            # Find the elements using XPath
            coupons = self.driver.find_elements(By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]')

            # Check the length of the found elements
            num_coupons = len(coupons)

            if(num_coupons > 0):
                copy_coupon_button =  WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]/div/div[3]/button[1]'))
            )
                copy_coupon_button.click()

                success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH,'//*[@id="notistack-snackbar"]'))).text

                self.assertEqual(success_message, 'Copied Coupon!')

            print("Successfully tested the Coupon copy button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Copy coupon button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_edit_coupon_flat_discount(self):
        print("\nStarted testing the edit coupon button with flat discount")

        try:

            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Click "Manage Coupon" button
            manage_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
            )
            manage_coupon_button.click() 

            # Find the elements using XPath
            coupons = self.driver.find_elements(By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]')

            # Check the length of the found elements
            num_coupons = len(coupons)

            if(num_coupons > 0):
                edit_coupon_button =  WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]/div/div[3]/button[2]'))
            )
                edit_coupon_button.click()

                reset_all_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[1]'))
            )
                reset_all_button.click() 

                # Find the input element for the discount code
                discount_code_input = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/form/div[1]/div/input'))
                )


                # Coupon with this discount coupon already exists
                discount_code = "NEW-CODE-CBE6"
                discount_code_input.send_keys(Keys.CONTROL + "a")
                discount_code_input.send_keys(discount_code)

                # Find the flat discount button
                flat_discount_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[2]/div/button[1]')

                # Click the flat discount button
                flat_discount_button.click()

                # Find the input element for the discount amount
                discount_amount_input = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[3]/div/input')

                # Select the current value in the input field
                discount_amount_input.send_keys(Keys.CONTROL + 'a')

                # Simulate pressing the backspace key to clear the value
                discount_amount_input.send_keys(Keys.BACKSPACE)

                # Enter a new discount amount
                discount_amount = "30"
                discount_amount_input.send_keys(discount_amount)

                # Find the input element for the number of students
                number_of_students = self.driver.find_element(By.XPATH, '//*[@id="num-of-students"]')

                # Enter the number of students
                number_of_students.send_keys(Keys.CONTROL + "a")
                number_of_students.send_keys(5)

                # Find the submit button
                submit_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[2]')

                # Click the submit button
                submit_button.click()

                popup_element = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))

                popup_text = popup_element.text
                expected_text = "Coupon code updated successfully"
                assert popup_text == expected_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}"

            print("Successfully tested the Edit coupon with flat discount")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Edit coupon with flat discount test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_edit_coupon_percentage_discount(self):
        print("\nStarted testing the edit coupon button with percentage discount")

        try:

            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Click "Manage Coupon" button
            manage_coupon_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[5]'))
            )
            manage_coupon_button.click() 

            # Find the elements using XPath
            coupons = self.driver.find_elements(By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]')

            # Check the length of the found elements
            num_coupons = len(coupons)

            if(num_coupons > 0):
                edit_coupon_button =  WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[1]/div/div[3]/button[2]'))
            )
                edit_coupon_button.click()

                reset_all_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[1]')))

                reset_all_button.click()

                # Find the input element for the discount code
                discount_code_input = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/form/div[1]/div/input'))
                )

                # Enter a new discount code
                # Generate a random suffix
                suffix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))

                # Coupon with this discount coupon already exists
                discount_code = "NEW-CODE-CBE6"
                discount_code_input.send_keys(Keys.CONTROL + "a")
                discount_code_input.send_keys(discount_code)

                # Find the percentage discount button
                percentage_discount_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[2]/div/button[2]')

                # Click the percentage discount button
                percentage_discount_button.click()

                # Find the input element for the discount amount
                discount_amount_input = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[3]/div/input')

                # Select the current value in the input field
                discount_amount_input.send_keys(Keys.CONTROL + 'a')

                # Simulate pressing the backspace key to clear the value
                discount_amount_input.send_keys(Keys.BACKSPACE)

                # Enter a new discount amount
                discount_amount = "30"
                discount_amount_input.send_keys(discount_amount)

                # Find the input element for the number of students
                number_of_students = self.driver.find_element(By.XPATH, '//*[@id="num-of-students"]')

                # Enter the number of students
                number_of_students.send_keys(Keys.CONTROL + 'a')
                number_of_students.send_keys(5)

                # Find the submit button
                submit_button = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/form/div[5]/button[2]')

                # Click the submit button
                submit_button.click()

                popup_element = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]')))

                popup_text = popup_element.text
                expected_text = "Coupon code updated successfully"
                assert popup_text == expected_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}"

            print("Successfully tested the Edit coupon with percentage discount")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Edit coupon with percentage discount test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_buttons_b_view_stats_button(self):
        print("\nStarted testing the view stats button")

        try:

            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
        
            view_stats_button = WebDriverWait(self.driver,20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[1]')))

            # Check if the "View Stats" button is displayed
            self.assertTrue(view_stats_button.is_displayed(), "View Stats button is not displayed.")

            # Check if the "View Stats" button is enabled
            self.assertTrue(view_stats_button.is_enabled(), "View Stats button is not enabled.")

            #Click the "View Stats" button
            view_stats_button.click()
            
            view_stats_dialog = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div')))

            self.assertTrue(view_stats_dialog.is_displayed(), "View Stats dialog is not visible")

            print("Successfully tested the view stats button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "View status button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_buttons_a_add_collaborator_button(self):
        print("\nStarted testing the Add collaborator button")

        try:

            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()
        
            add_collaborator_button = WebDriverWait(self.driver,20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[1]/div[6]/button[1]')))

            # Check if the "Add collaborator" button is displayed
            self.assertTrue(add_collaborator_button.is_displayed(), "Add collaborator button is not displayed.")

            # Check if the "Add collaborator" button is enabled
            self.assertTrue(add_collaborator_button.is_enabled(), "Add collaborator button is not enabled.")

            #Click the "Add collaborator" button
            add_collaborator_button.click()
            
            add_collaborator_dialog = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div')))

            self.assertTrue(add_collaborator_button.is_displayed(), "Add collaborator dialog is not visible")

            print("Successfully tested the add collaborator button")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Add collaborator button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_add_module(self):
        print("\nStarted testing the Add Module button")

        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Wait for the "Add Module" button to be clickabl
            add_module_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="bar_ref"]/div[3]'))
            )

            # Scroll to find the "Add Module" button
            self.driver.execute_script("arguments[0].scrollIntoView(true);", add_module_button)

            # Click on the "Add Module" button using JavaScript
            self.driver.execute_script("arguments[0].click();", add_module_button)
            
            print("\nStarted testing the Submit button")

            # Wait for the module title input to be visible
            module_title_input = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/input'))
            )

            # Input a value in the module title field
            test_input = "New Test Module"
            module_title_input.send_keys(test_input)

            # Wait for the submit button to be clickable
            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[3]/div/div[3]/button"))
            )

            # Click on the submit button using JavaScript
            self.driver.execute_script("arguments[0].click();", submit_button)

            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div/div[3]"))
            )

            self.assertEqual(success_message.text,"Module is added successfully")

            print("Successfully tested the Add module button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Add module button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_delete_module(self):
        print("\nStarted testing the Delete Module button")

        try:
            # Navigate to a paid course
            self.course_page.navigate_to_a_paid_course()

            # Close the first module (optional step)
            first_module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="module_1"]'))
            )
            close_module = WebDriverWait(first_module, 20).until(
                EC.element_to_be_clickable((By.XPATH, './/div[2]/span[1]'))
            )
            close_module.click()

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_8'))
            )

            # Find the dropdown button within the module
            dropdown_button = WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'dropdown-btn'))
            )

            # Scroll the dropdown button into view
            self.driver.execute_script("arguments[0].scrollIntoView(true);", dropdown_button)

            # Click on the dropdown button using JavaScript
            self.driver.execute_script("arguments[0].click();", dropdown_button)

            # Perform action for the delete button
            options = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'MuiTooltip-tooltip'))
            )

            # Find the delete button for the module
            buttons = options.find_elements(By.CLASS_NAME, 'MuiIconButton-root')
            delete_button = buttons[2]

            # Scroll the delete button into view
            self.driver.execute_script("arguments[0].scrollIntoView(true);", delete_button)

            # Click on the delete button using JavaScript
            self.driver.execute_script("arguments[0].click();", delete_button)

            # Wait for the success message
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div/div[3]"))
            )

            # Assert the success message
            self.assertEqual(success_message.text, "Module is deleted successfully")

            print("Successfully tested the Delete module button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Delete module button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_a_video(self):
        print("\nStarted testing the Module Content-Video button")

        try:
            
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the video content button
            video_content = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="Learn+JavaScript+by+Building+7+Games+-+Full+Course26a7d"]/div[1]')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", video_content)

            #Find the current url of the page
            current_url = self.driver.current_url

            # Check if the video identifier is present in the URL
            assert 'video' in current_url, "The URL does not contain the keyword 'video'"

            print("Successfully tested the Module content-Video")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Video button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_b_pdf_reading_material(self):
        print("\nStarted testing the Module Content- PDF Reading material button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)
            
            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the pdf reading material button
            pdf_reading_material = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_readingMaterialDescription")]')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", pdf_reading_material)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("External PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("External PDF not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-PDF Reading material button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-PDF Reading material button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_c_external_link_reading_material(self):
        print("\nStarted testing the Module Content- External link Reading material button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the external link reading material button
            external_link_reading_material = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div[2]/div/div[3]/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", external_link_reading_material)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("External PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("External PDF not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-External link Reading material button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-External link Reading material button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_d_time_for_a_quiz(self):
        print("\nStarted testing the Module Content-Time for a quiz! button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the 'Time for a quiz' button
            time_for_a_quiz_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div[2]/div/div[4]/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", time_for_a_quiz_button)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("Quiz PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("Quiz Pdf not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-Time for a Quiz button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content- Time for a quiz button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_e_demo_project_view_submissions(self):
        print("\nStarted testing the Module Content-Demo project View Submissions")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_projectTitle")]'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Check if the projects dialog is displayed
            projects_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert projects_dialog.is_displayed(), "The projects dialog is not displayed"

            # Find and click the 'View Submissions' button
            view_submissions_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/button'))
            )
            view_submissions_button.click()

            # Check if the Submit Scores dialog is displayed
            submit_scores_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert submit_scores_dialog.is_displayed(), "The Submit scores dialog is not displayed"
            
            print("Successfully tested the Module Content-Demo Project View Submissions")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Project View Submissions test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_f_demo_project_award_marks(self):
        print("\nStarted testing the Module Content-Demo project Award Marks")

        try:
            
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_projectTitle")]'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Find and click the 'Award Marks' button
            view_submissions_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/button'))
            )
            view_submissions_button.click()

            # Check if the Submit Scores dialog is displayed
            submit_scores_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            submission = WebDriverWait(submit_scores_dialog, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/table/tbody/tr'))
            )

            award_marks_button = WebDriverWait(submission, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/table/tbody/tr/td[3]/div'))
            )
            award_marks_button.click()

            percentage_options = self.driver.find_elements(By.XPATH, '//*[@id="menu-"]/div[3]/ul/li')
            first_li_element = percentage_options[1]

            # Check if the option is already selected
            is_selected = first_li_element.get_attribute("aria-selected")
            if not is_selected:
                first_li_element.click()

            # Wait for the success message dialog to be visible or timeout
            try:
                success_message = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
                )

                # Get the text of the success message
                success_message_text = success_message.text

                # Assert the expected text
                expected_text = "Grade Updated Successfully"
                assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            except TimeoutException:
                # Handle the case when no success message is displayed
                print("Option is already selected. No success message displayed.")

            print("Successfully tested the Module Content-Demo Project Award Marks")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Project Award Marks test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_g_demo_project_add_remarks(self):
        print("\nStarted testing the Module Content-Demo project Add Remarks")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_projectTitle")]'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Find and click the 'Award Marks' button
            view_submissions_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/button'))
            )
            view_submissions_button.click()

            # Check if the Submit Scores dialog is displayed
            submit_scores_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            submission = WebDriverWait(submit_scores_dialog, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/table/tbody/tr'))
            )

            add_remarks_button = WebDriverWait(submission, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/table/tbody/tr/td[4]/button'))
            )
            add_remarks_button.click()

            add_remark_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            remarks_input = WebDriverWait(add_remark_dialog, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[2]/div[2]/div'))
            )
            # Generate a random suffix
            suffix = ''.join(random.choices(string.ascii_lowercase,k=2))
            text_to_enter = "Nice work" + suffix
            remarks_input.send_keys(Keys.CONTROL + "a")
            remarks_input.send_keys(Keys.BACKSPACE)
            remarks_input.send_keys(text_to_enter)

            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary'))
            )
            self.driver.execute_script("arguments[0].click();", submit_button)

            success_message = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
                )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Remark Updated Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            
            print("Successfully tested the Module Content-Demo Project Add Remarks")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Project Add Remarks test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    

    def test_existing_module_content_h_demo_question_view_answer(self):
        print("\nStarted testing the Module Content-Demo question View Answer button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div[2]/div/div[6]/div/p'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'View Answer' button
            view_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[1]/span/button'))
            )
            view_answer_button.click()

            # Check if the All Answers dialog is displayed
            all_answers_dialog = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert all_answers_dialog.is_displayed(), "The All answers dialog is not displayed"

            close_all_answer_dialog_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-title"]/button/span[1]'))
            )
            self.driver.execute_script("arguments[0].click();", close_all_answer_dialog_button)

            print("Successfully tested the Module Content-Demo Question View answer button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Question View answer test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_existing_module_content_i_demo_question_add_evaluation(self):
        print("\nStarted testing the Module Content-Demo question Add Evaluation")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div[2]/div/div[6]/div/p'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'View Answer' button
            view_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[1]/span/button'))
            )
            view_answer_button.click()

            # Check if the All Answers dialog is displayed
            all_answers_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert all_answers_dialog.is_displayed(), "The All answers dialog is not displayed"

            answer = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]'))
            )

            #Find the 'Add Evaluation button'
            add_evaluation_button = WebDriverWait(answer, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[2]/div/label/div'))
            )

            add_evaluation_button.click()

            evaluation_options = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="menu-"]/div[3]/ul/li'))
            )
            
            # Verify that the evaluation options contain "Not Evaluated", "Correct" and "Incorrect"
            expected_options = ["Not Evaluated", "Correct", "Incorrect"]

            # Get the text of each evaluation option
            evaluation_option_texts = [option.text for option in evaluation_options]
            
            # Verify the presence of expected options in the dropdown
            for option in expected_options:
                assert option in evaluation_option_texts, f"Expected option '{option}' not found in the evaluation options."

            
            select_one_option = evaluation_options[2]
            self.driver.execute_script("arguments[0].click();", select_one_option)

        
            print("Successfully tested the Module Content-Demo Question Add Evaluation")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Question Add Evaluation test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_existing_module_content_j_vote_answer(self):
        print("\nStarted testing the Module Content - Vote Answer")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="<p>demo-questio-496c8"]/div/p/span/p'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'View Answer' button
            view_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[1]/span/button'))
            )
            view_answer_button.click()

            # Check if the All Answers dialog is displayed
            all_answers_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert all_answers_dialog.is_displayed(), "The All answers dialog is not displayed"

            answer = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]'))
            )

            vote = WebDriverWait(answer, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'QA-comment-pdf-vote'))
            )

            vote_buttons = vote.find_elements(By.TAG_NAME, 'svg')
            upvote_button = vote_buttons[0]

            # Check if the upvote button is clickable
            WebDriverWait(vote, 10).until(EC.element_to_be_clickable((By.TAG_NAME, 'svg')))

            # Click the upvote button
            upvote_button.click()

            downvote_button = vote_buttons[1]

            # Check if the downvote button is clickable
            WebDriverWait(vote, 10).until(EC.element_to_be_clickable((By.TAG_NAME, 'svg')))

            downvote_button.click()

            print("Successfully tested the Module Content - Vote Answer")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Vote Answer test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)



    def test_existing_module_content_k_demo_question_add_answer(self):
        print("\nStarted testing the Module Content-Demo question Add Answer button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div[2]/div/div[6]/div/p'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'Add Answer' button
            add_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[2]/span/button'))
            )
            add_answer_button.click()

            # Locate the div element
            answer_textbox = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div')
            
            # Interact with the div element (e.g., clear the content)
            answer_textbox.send_keys(Keys.CONTROL + "a")
            answer_textbox.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "This is my answer" + suffix

            # Enter text into the div element
            answer_textbox.send_keys(text_to_enter)

            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary'))
            )
            self.driver.execute_script("arguments[0].click();", submit_button)

            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-text'))
            )

            assert success_message.text == "Answer submitted successfully"

            print("Successfully tested the Module Content-Demo Question Add answer button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Question Add answer test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_module_content_a_video(self):
        print("\nStarted testing the Module Content Video - Add, Edit, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)
            
            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            add_content_button =  WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            video_index = 1  # Index starts from 1

            self.course_page.select_module_content_by_index(video_index)

            video_input_field = module.find_element(By.CLASS_NAME, 'add-video-input')
            text_to_enter = test_youtube_url
            video_input_field.clear()
            video_input_field.send_keys(text_to_enter)

            # Find the fetch button
            fetch_button = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'fetch-btn')))

            self.driver.execute_script("arguments[0].click();", fetch_button)

            # Locate and click the submit button
            submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/button')))

            submit_button.click()
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Video added successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Add Module Content - Video")
            time.sleep(3)

            #----------Testing the Edit feature of Module Content - Video----------

            edit_video_button = WebDriverWait(module,10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="Student+Testimonial+||+BeyondExams1a0aa"]/div[1]/button[2]')))
            self.driver.execute_script("arguments[0].click();", edit_video_button)

            edit_video_dialog = WebDriverWait(module, 10).until(
                EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            video_title_input_field = edit_video_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/input')
            text_to_enter = "Learn JavaScript by Building 7 Games - Full Course"
            video_title_input_field.clear()
            video_title_input_field.send_keys(text_to_enter)

            video_description_input_field = edit_video_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[4]/textarea')
            text_to_enter = "Learn JavaScript by building 7 retro games."
            video_description_input_field.clear()
            video_description_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(edit_video_dialog, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))
            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Video updated successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Edit Module Content - Video")
            time.sleep(3)

            #----------Testing the Delete feature of Module Content - Video----------

            video_content_element = WebDriverWait(module, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//div[contains(@id, "Student+Testimonial+||+BeyondExams")]'))
            )

            delete_video_button = WebDriverWait(video_content_element, 20).until(
                EC.element_to_be_clickable((By.XPATH,'./div/button[1]')))

            self.driver.execute_script("arguments[0].click();", delete_video_button)

            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)

            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Video Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - Video")

            print("Successfully tested the Module Content Video - Add, Edit, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Video - Add, Edit, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_module_content_b_pdf_reading_material_browse_file(self):
        print("\nStarted testing the Module Content - Reading Material Using Browse File option - Add, Edit, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            add_content_button =  WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            pdf_reading_material_index = 2  # Index starts from 1

            self.course_page.select_module_content_by_index(pdf_reading_material_index)

            title_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[1]/input')
            text_to_enter = 'PDF Reading Material'
            title_input_field.send_keys(text_to_enter)

            description_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[2]/input')
            text_to_enter = 'PDF Reading Material'
            description_input_field.send_keys(text_to_enter)

            select_option = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/button')))
            
            self.driver.execute_script("arguments[0].click();", select_option)

            browse_file_option = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[3]/div/ul/li[1]')))

            self.driver.execute_script("arguments[0].click();", browse_file_option)

            file_upload_input = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/span')

            self.driver.execute_script("arguments[0].click();", file_upload_input)

            # Wait for the file selection dialog to appear
            pyautogui.sleep(1)

            # Set the file path to be uploaded
            file_path = path = testing_pdf

            # Type the file path in the file selection dialog
            pyautogui.write(file_path)

            # Press Enter to confirm the file selection
            pyautogui.press("enter")

            time.sleep(5)
            # Find the submit button
            submit_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/button'))
            )

            # Click the submit button
            self.driver.execute_script("arguments[0].click();", submit_button)
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Added Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Add Module Content - Reading Material Using Browse File option")
            time.sleep(5)

            #----------Testing Edit Module Content - Reading Material Using Browse File option----------

            edit_pdf_reading_material_button = WebDriverWait(module,10).until(EC.element_to_be_clickable((By.XPATH,'//*[contains(@id, "pdf-reading-mat")]/div/button[2]')))
            self.driver.execute_script("arguments[0].click();", edit_pdf_reading_material_button)

            edit_pdf_reading_material_dialog = WebDriverWait(module, 10).until(
                EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            reading_material_title_input_field = edit_pdf_reading_material_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/input')
            text_to_enter = "Pdf Reading Material"
            reading_material_title_input_field.clear()
            reading_material_title_input_field.send_keys(text_to_enter)

            reading_material_description_input_field = edit_pdf_reading_material_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[4]/textarea')
            text_to_enter = "Pdf Reading Material"
            reading_material_description_input_field.clear()
            reading_material_description_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(edit_pdf_reading_material_dialog, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))
            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Edited Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Edit Module Content - Pdf Reading Material")
            time.sleep(5)

            #----------Testing Delete Module Content----------

            pdf_reading_material_element = WebDriverWait(module, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//div[contains(@id, "pdf-reading-mat")]'))
            )
            
            delete_pdf_reading_material_button = WebDriverWait(pdf_reading_material_element, 20).until(
                EC.element_to_be_clickable((By.XPATH,'./div/button[1]')))

            self.driver.execute_script("arguments[0].click();", delete_pdf_reading_material_button)
            
            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)


            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - Pdf Reading Material")

            print("Successfully tested the Module Content Reading Material Using Browse File option - Add, Edit, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Reading Material Using Browse File option - Add, Edit, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


    def test_new_module_content_c_pdf_reading_material_Upload_Url(self):
        print("\nStarted testing the Module Content - Reading Material Using Upload url option - Add, Edit, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )
            
            add_content_button =  WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            pdf_reading_material_index = 2  # Index starts from 1

            self.course_page.select_module_content_by_index(pdf_reading_material_index)

            title_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[1]/input')
            text_to_enter = 'External link PDF Reading Material'
            title_input_field.send_keys(text_to_enter)

            description_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[2]/input')
            text_to_enter = 'External link PDF Reading Material'
            description_input_field.send_keys(text_to_enter)

            select_option = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/button')))
            
            self.driver.execute_script("arguments[0].click();", select_option)

            upload_url_option = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/div[3]/div/ul/li[2]')))

            self.driver.execute_script("arguments[0].click();", upload_url_option)

            add_url_input = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/input')

            url_to_enter = 'https://beltline.org/wp-content/uploads/2020/09/placeholder-pdf.pdf'
            add_url_input.send_keys(url_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[4]/label/div/button')))

            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Added Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Add Module Content - Reading Material Using Upload url option")
            time.sleep(3)

            #----------Testing Edit Module Content - Reading Material Using Upload url option----------

            external_link_pdf_reading_material_element = WebDriverWait(module, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiGrid-item'))
            )

            buttons = WebDriverWait(external_link_pdf_reading_material_element, 10).until(
                EC.visibility_of_all_elements_located((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
          
            edit_pdf_reading_material_button = buttons[2]
            self.driver.execute_script("arguments[0].click();", edit_pdf_reading_material_button)


            edit_pdf_reading_material_dialog = WebDriverWait(module, 10).until(
                EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            reading_material_title_input_field = edit_pdf_reading_material_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/input')
            text_to_enter = "External link Pdf Reading Material"
            reading_material_title_input_field.clear()
            reading_material_title_input_field.send_keys(text_to_enter)

            reading_material_description_input_field = edit_pdf_reading_material_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[4]/textarea')
            # Generate a random suffix
            suffix = ''.join(random.choices(string.ascii_lowercase,k=2))
            text_to_enter = "External link Pdf Reading Material for test"+suffix
            reading_material_description_input_field.clear()
            reading_material_description_input_field.send_keys(text_to_enter)

            reading_material_link_input_field = edit_pdf_reading_material_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[6]/input')
            text_to_enter = "https://beltline.org/wp-content/uploads/2020/09/placeholder-pdf.pdf"
            reading_material_link_input_field.clear()
            reading_material_link_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(edit_pdf_reading_material_dialog, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))
            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Edited Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Edit Module Content - External link Pdf Reading Material")

            time.sleep(3)

            #----------Testing Delete Module Content - External link Pdf Reading Material----------

            delete_pdf_reading_material_button = buttons[1]
            self.driver.execute_script("arguments[0].click();", delete_pdf_reading_material_button)

            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Reading Material Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - External link PDF Reading Material")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - External link PDF Reading Material - Add, Edit, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_module_d_content_quiz(self):
        print("\nStarted testing the Module Content - Quiz - Add, Edit, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            add_content_button =  WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            quiz_index = 3  # Index starts from 1

            self.course_page.select_module_content_by_index(quiz_index)

            quiz_link_input = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[7]/div/input')
            text_to_enter = 'https://drive.google.com/file/d/1y8H-PU1a-oxRWjYJzI0ai8w7VLmYgOsA/view?usp=drive_link'
            quiz_link_input.send_keys(text_to_enter)

            # Locate and click the Add quiz button
            add_quiz_button = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[7]/div/button')))

            self.driver.execute_script("arguments[0].click();", add_quiz_button)

            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Quiz added successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Add Module Content - Quiz")
            time.sleep(3)

            #----------Testing Edit Module Content - Quiz----------
            quiz_element = WebDriverWait(module, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiGrid-item'))
            )

            buttons = WebDriverWait(quiz_element, 10).until(
                EC.visibility_of_all_elements_located((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
          
            edit_quiz_button = buttons[2]
            self.driver.execute_script("arguments[0].click();", edit_quiz_button)

            edit_quiz_dialog = WebDriverWait(module, 10).until(
                EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            quiz_url_input_field = edit_quiz_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/div/input')
            text_to_enter = "https://beltline.org/wp-content/uploads/2020/09/placeholder-pdf.pdf"
            quiz_url_input_field.clear()
            quiz_url_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(edit_quiz_dialog, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))
            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Quiz edited successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Edit Module Content - Quiz")
            time.sleep(3)

            #----------Testing Delete Module Content - Quiz----------
            
            delete_quiz_content_button = buttons[1]

            self.driver.execute_script("arguments[0].click();", delete_quiz_content_button)
        
            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)


            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )
           
            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Quiz Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - Quiz")

            print("Successfully tested Module Content - Quiz - Add, Edit, Delete")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Quiz - Add, Edit, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


    def test_add_module_content_e_project(self):
        print("\nStarted testing the Add Module Content - Project - Add, Edit, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            # Click on the 'Add Content' button
            add_content_button = WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            project_index = 4  # Index starts from 1

            # Select the project option from the dropdown
            self.course_page.select_module_content_by_index(project_index)

            # Wait for the 'Add Project' dialog to appear
            add_project_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Find the input field for the project and enter text
            add_project_input = WebDriverWait(add_project_dialog, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/div[2]/div[2]/div'))
            )
            text_to_enter = 'Demo Project - For Testing Purpose'
            add_project_input.send_keys(text_to_enter)

            # Locate and click the Submit button
            submit_button = WebDriverWait(add_project_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary'))
            )
            self.driver.execute_script("arguments[0].click();", submit_button)

            notification =  WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notificationBox"]'))
            )

            # Get the text of the notification
            notification_text = notification.text

            # Assert the expected text
            expected_text = "You got 5 keys for adding a project."
            assert  expected_text in notification_text, f"Expected text: '{expected_text}', Actual text: '{notification_text}'"
            print("Successfully tested the Add Module Content - Project")
            time.sleep(3)

            #----------Testing Edit Module Content - Project----------

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_projectTitle")]'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Check if the projects dialog is displayed
            projects_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Wait for the desired button to be clickable
            edit_project_button = WebDriverWait(projects_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiSvgIcon-root:nth-child(3)'))
            )
            edit_project_button.click()

            edit_project_dialog = WebDriverWait(module, 10).until(
                EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            project_name_input_field = WebDriverWait(edit_project_dialog, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div/div[2]/div[2]/div')))
            
            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=2))
            
            text_to_enter = "Demo project for Testing Purpose" + suffix
            # Select all the existing text and delete it
            project_name_input_field.send_keys(Keys.CONTROL + "a")
            project_name_input_field.send_keys(Keys.BACKSPACE)
            project_name_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))
            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Project Edited Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Edit Module Content - project")
            time.sleep(3)

            #----------Testing Delete Module Content - Project----------
            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, './/*[starts-with(@class, "Module_projectTitle")]'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Check if the projects dialog is displayed
            projects_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Wait for the desired button to be clickable
            delete_project_button = WebDriverWait(projects_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiSvgIcon-root:nth-child(2)'))
            )
            delete_project_button.click()

            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Project removed successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - project")

            print("Successfully tested the Module Content - project - Add, Edit, Delete")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - project - Add, Edit, Delete test failed"
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    
    def test_new_module_content_f_question(self):
        print("\nStarted testing the Module Content - Question - Add, Delete")

        try:
           
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            # Click on the 'Add Content' button
            add_content_button = WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            question_index = 5  # Index starts from 1

            # Select the project option from the dropdown
            self.course_page.select_module_content_by_index(question_index)

            # Wait for the 'Add Question' dialog to appear
            add_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Find the input field for the question and enter text
            add_question_input = WebDriverWait(add_question_dialog, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div'))
            )
            text_to_enter = 'Demo Question - For Testing Purpose'
            add_question_input.send_keys(text_to_enter)

            # Locate and click the Submit button
            submit_button = WebDriverWait(add_question_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary'))
            )
            self.driver.execute_script("arguments[0].click();", submit_button)
            
            print("Successfully tested the Add Module Content - Question")
            time.sleep(3)

            #----------Testing Delete Module Content - Question----------

            demo_question = WebDriverWait(module, 20).until(
                EC.visibility_of_element_located((By.XPATH, '//div[contains(@id, "demo-questio")]'))
            )

            buttons = demo_question.find_elements(By.CSS_SELECTOR,'.MuiSvgIcon-root')

            # Click on the 'delete' button
            delete_question_button = buttons[1]
            
            delete_question_button.click()

            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)

            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Question Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - Question")

            print("Successfully tested the Module Content - Question - Add, Delete")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Question - Add, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_new_module_g_embed_content(self):
        print("\nStarted testing the Module Content - Embed Content - Add, Delete")

        try:
            # Navigate to the module
            self.driver.get(new_module_test_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_7'))
            )

            add_content_button =  WebDriverWait(module, 20).until(
                EC.element_to_be_clickable((By.CLASS_NAME, 'enrollBtn'))
            )
            self.driver.execute_script("arguments[0].click();", add_content_button)

            embed_content_index = 6  # Index starts from 1

            self.course_page.select_module_content_by_index(embed_content_index)

            title_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[5]/div[1]/input')
            text_to_enter = 'Embed Content'
            title_input_field.send_keys(text_to_enter)

            description_input_field = module.find_element(By.XPATH, '//*[@id="41067"]/div[1]/div[5]/div[2]/input')
            text_to_enter = 'Embed Content'
            description_input_field.send_keys(text_to_enter)

            #Embedding content using an URL
            embed_content_input_field = module.find_element(By.ID,'embed_content')
            text_to_enter = 'https://twitter.com/IGN/status/1602423321156632578?s=20&t=4NPRZ13nDWpAztnDsZBAbw'
            embed_content_input_field.send_keys(text_to_enter)
            
            # Locate and click the submit button
            submit_button = WebDriverWait(module, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="41067"]/div[1]/div[5]/label/div/button')))

            self.driver.execute_script("arguments[0].click();", submit_button)

            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Content Embedded Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Add Module Content - Embed Content")
            time.sleep(5)

            #----------Testing Delete Module Content - Embed Content----------

            embed_content_element = WebDriverWait(module, 20).until(
                EC.visibility_of_element_located((By.XPATH, '//div[contains(@id, "embed-content")]'))
            )

            buttons = embed_content_element.find_elements(By.CSS_SELECTOR,'.MuiSvgIcon-root')

            delete_embed_content_button = buttons[1]
            delete_embed_content_button.click()

            warning_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
            )

            # Find and click the 'Yes button' in warming popup
            yes_button = WebDriverWait(warning_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", yes_button)
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Embed Content Deleted Successfully"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"
            print("Successfully tested the Delete Module Content - Embed Content")

            print("Successfully tested Embed Content - Add, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content - Embed Content - Add, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


    
    
    

if __name__ == '__main__':
    
    unittest.main()
        