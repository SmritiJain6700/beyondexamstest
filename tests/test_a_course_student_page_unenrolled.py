import unittest
import logging
import time
import json
import sys
import string
import random
import traceback
import pyautogui

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url
from helpers import wait_for_pageload, create_driver, login
from selenium.webdriver.common.keys import Keys
from data.test_data import valid_coupon_code, invalid_coupon_code,unerolled_course_url
from data.test_data import test_file_path

# Configure logging to write to a file
logging.basicConfig(filename='error_for_a_course_student_page.log',level=logging.ERROR)

class CourseUnEnrolledStudentPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)
        
        # Check if the role of the user is "teacher"
        if self.course_page.get_user_role() == "T":
            # Switch the role to "teacher"
            self.course_page.switch_role()
        
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    @classmethod
    def setUp(self):
        time.sleep(2)
        
    def test_get_in_touch_button(self):
        print("\nStarted testing the Get In Touch button")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Locate the Get In Touch button
            get_in_touch_button_path = '//*[@id="root"]/div[8]/div/div[10]/div/div[2]/button/a'
            
            # Wait for the Get In Touch button to become clickable
            get_in_touch_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, get_in_touch_button_path)))

            # Scroll the Get In Touch button into view
            self.driver.execute_script("arguments[0].scrollIntoView();", get_in_touch_button)

            # Click the Get In Touch button using JavaScript
            self.driver.execute_script("arguments[0].click();", get_in_touch_button)
            
            # Get the window handles
            window_handles = self.driver.window_handles

            # Switch to the new tab
            self.driver.switch_to.window(window_handles[1])
            
            # Get the current URL
            current_url = self.driver.current_url

            # Assert if "whatsapp" is present in the current URL
            assert "whatsapp" in current_url, "Failed to find 'whatsapp' in the current URL."

            print("Successfully tested get in touch button.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Get in touch button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
        
    def test_help_button_chat_with_us(self):
        print("\nStarted testing the help button - Chat with us")

        try:
                
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            chat_with_us_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a/div'))
            )
            chat_with_us_button.click()

            # Verify the URL contains "support" indicating the chat window is open
            self.assertIn("Support", self.driver.current_url)

            # Verify the chat window header text
            chat_header = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[1]/div[1]/div[1]/h4'))
            )
            self.assertEqual(chat_header.text, "Beyondexams Support")

            print("Successfully tested help button - Chat with us")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Chat with us test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_help_button_submit_feedback(self):
        print("\nStarted testing the help button - Submit Feedback")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            submit_feedback_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[1]/div'))
            )
            submit_feedback_button.click()

            feedback_form =  WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
                )
            
            feedback_input = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="feedback"]')))
            feedback_input.clear()  # Clear any existing value
            feedback_input.send_keys("This is a test feedback")  # Enter the test feedback
            
            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/button')))
            
            submit_button.click()
            
            # Verify if the expected error message is displayed
            success_message = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[3]'))).text

            expected_message = "Feedback added successfully"
            assert success_message == expected_message, f"Element text '{success_message}' does not match expected text '{expected_message}'"

            print("Successfully tested the Submit feedback functionality")
        
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Submit Feedback test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
        
    def test_help_button_report_a_problem(self):
        print("\nStarted testing help button - Report a problem")
        
        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            report_a_problem_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]'))
            )
            report_a_problem_button.click()

            report_form = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            self.assertTrue(report_form.is_displayed())

            print("Successfully tested help button - Report a problem")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Report a problem test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_chat_with_course_instructor(self):
        print("\nStarted testing watch tutorial - Chat with course instructor")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the chat with course instructor button to appear
            chat_with_course_instructor_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[1]/div/div'))
            )
            chat_with_course_instructor_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text

            expected_video_player_title = "Connect with your course instructor on Beyond exams platform."
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()

            print("Successfully tested watch tutorial - Chat with course instructor")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Chat with course instructor test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

                                  
    def test_clear_your_doubts(self):
        print("\nStarted testing watch tutorial - Clear your doubts")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the clear your doubts button to appear
            clear_your_doubts_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[2]/div/div'))
            )
            clear_your_doubts_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text

            expected_video_player_title = "Ask course related queries on Beyond exams platform."
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()

            print("Successfully tested watch tutorial - Clear your doubts")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Clear your doubts test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    
    def test_set_your_learning_targets(self):
        print("\nStarted testing watch tutorial - Set your learning targets")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the Set your learning targets button to appear
            set_your_learning_targets_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[3]/div/div'))
            )
            set_your_learning_targets_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text
            
            expected_video_player_title = "Set targets to learn on beyond exams website."
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()

            print("Successfully tested watch tutorial - Set your learning targets")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Set your learning targets test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    def test_complete_a_project(self):
        print("\nStarted testing watch tutorial - Complete a project")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the Complete a project button to appear
            complete_a_project_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[4]/div/div'))
            )
            complete_a_project_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text
        
            expected_video_player_title = "Learn to work on projects without even enrolling into BE course."
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()

            print("Successfully tested watch tutorial - Complete a project")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Complete a project test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    def test_your_BE_activities(self):
        print("\nStarted testing watch tutorial - Your BE activities")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the Your BE activities button to appear
            your_BE_activities_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[5]/div/div'))
            )
            your_BE_activities_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text
            
            expected_video_player_title = "Learn to keep a track of your performance on BE platform!"
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()

            print("Successfully tested watch tutorial - Your BE activities")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Your BE activities test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    
    def test_refer_and_earn(self):
        print("\nStarted testing watch tutorial - Refer and Earn")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Click on the button
            help_button.click()

            # Wait for the Watch Tutorial option to appear
            watch_tutorial = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[3]/div')))

            # Click on the button
            watch_tutorial.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Wait for the Refer and Earn button to appear
            refer_and_earn_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a[6]/div/div'))
            )
            refer_and_earn_button.click()

            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)

            # Check the video player title
            video_player = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="movie_player"]/div[3]/div[2]/div'))
            )

            video_player_title = video_player.text
        
            expected_video_player_title = "Learn to refer your friends and stand a chance to earn T-shirt on BE."
            self.assertEqual(video_player_title, expected_video_player_title)

            self.driver.switch_to.default_content()
            
            print("Successfully tested watch tutorial - Refer and Earn")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Refer and Earn test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)    
    
    def test_course_a_apply_valid_coupon_code(self):
        print("\nStarted testing the application of a valid coupon code")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Click the "Apply Coupon" button
            apply_coupon_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/div/button')))
            apply_coupon_button.click()
            
            # Enter the coupon code in the input field
            coupon_input_field = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/input')))
            coupon_input_field.clear()  # Clear any existing value in the input field
            coupon_input_field.send_keys(valid_coupon_code)  # Enter your desired coupon code
            
            # Submit the coupon
            coupon_submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/button')))
            coupon_submit_button.click()
            
            # Verify if the coupon was successfully applied
            successfully_applied_text = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/p[1]'))).text

            expected_text = "Coupon Applied"
            assert successfully_applied_text == expected_text, f"Element text '{successfully_applied_text}' does not match expected text '{expected_text}'"

            print("Successfully tested application of a valid coupon code.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Apply a valid coupon code test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
        
    def test_course_b_apply_invalid_coupon_code(self):
        print("\nStarted testing the application of an invalid coupon code")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Click the "Apply Coupon" button
            apply_coupon_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/div/button')))
            apply_coupon_button.click()

            # Enter an invalid coupon code in the input field
            coupon_input_field = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/input')))
            coupon_input_field.clear()  # Clear any existing value in the input field
            coupon_input_field.send_keys(invalid_coupon_code)  # Enter the invalid coupon code

            # Submit the coupon
            coupon_submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/button')))
            coupon_submit_button.click()

            # Verify if the expected error message is displayed
            error_message = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[3]'))).text

            expected_message = "Coupon code doesnt exist"
            assert error_message == expected_message, f"Element text '{error_message}' does not match expected text '{expected_message}'"

            print("Successfully tested the application of an invalid coupon code.")
        
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Apply a invalid coupon code test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_course_c_if_trailer_play_button(self):
        print("\nStarted testing the trailer play button")
        
        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            trailer_play_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[1]/div[1]/button')))
            trailer_play_button.click()
            
            # Switch to the iframe context
            iframe = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="widget2"]')))
            self.driver.switch_to.frame(iframe)
                 
            # Wait for the video player to be present
            video_player = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, 'video')))

            # Check if the video is playing
            is_playing = self.driver.execute_script('return arguments[0].paused;', video_player) 
            if is_playing:
                print("The video is not playing.")
            else:
                print("The video is playing.")

            # Switch back to the default content
            self.driver.switch_to.default_content()

            print("Successfully tested the trailer play button")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Trailer play button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
        
    def test_course_d_talk_to_us_button(self):
        #for this course the notifications are already enabled
        print("\nStarted testing talk to us button")
        
        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Click the "Share" button
            talk_to_us_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[1]/button[2]')))
            talk_to_us_button.click()

            # Find the input field by its ID
            input_field = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="send-message-text"]')))

            self.driver.execute_script("arguments[0].scrollIntoView();", input_field)

            # Enter the desired text into the input field
            text_to_enter = "This is a new test"
            input_field.send_keys(text_to_enter)
            
            # Find the send button by its class name and SVG tag name
            send_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".pointer svg")))

            # Click the send button
            send_button.click()

            # Wait for the sent message to be displayed in the message box
            sent_message = WebDriverWait(self.driver, 10).until(EC.text_to_be_present_in_element((By.CLASS_NAME, "chat-box-container-left"), text_to_enter))

            # Assertion to check if the sent message is displayed
            assert sent_message, "Sent message not displayed in the message box"

            # If the sent message is displayed, the test passes
            print("Sent message is displayed in the message box.")

            print("Successfully tested the talk to us button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Talk to us button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        

    def test_course_d_share_link_copied_successfully(self):
        print("\nStarted testing Course Share Link Generation")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Click the "Share" button
            share_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/div/div/button')))
            share_button.click()

            # Click copy link button
            copy_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div')))
            copy_button.click()

            success_message = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))).text

            # Verify if the copied link matches the generated link
            print(success_message)
            assert success_message == "Link Copied Successfully"

            print("Successfully copied the course share link")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Copy Course link test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_course_e_share_dialog_social_media(self):
        print("\nStarted testing Share Dialog Social Media")

        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)
            
            # Click the "Share" button
            share_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/div/div/button')))
            share_button.click()

            # Wait for the dialog to open
            dialog = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div')

            # Verify the working of social media platform icons
            social_media_icons = dialog.find_elements (By.XPATH, '//*[@id="common-dialog-description"]/div[1]/button')
            expected_icons = {
                'facebook': 'facebook.com',
                'linkedin': 'linkedin.com',
                'twitter': 'twitter.com',
                'instagram': 'instagram.com',
                'whatsapp': 'whatsapp.com'
            }

            # Click each social media platform icon and validate the behavior
            for icon in social_media_icons:
                icon_name = icon.get_attribute('aria-label')
                icon.click()

                window_handles = self.driver.window_handles

                # Switch to the new window or tab
                self.driver.switch_to.window(window_handles[-1])

                # Validate the expected behavior for each social media platform
                if icon_name in expected_icons:
                    expected_url = expected_icons[icon_name]
                    assert expected_url in self.driver.current_url, f"{icon_name.capitalize()} did not open in a new tab."

                # Close the new window or tab
                self.driver.close()

                # Switch back to the original window or tab
                self.driver.switch_to.window(window_handles[0])

            print("Successfully tested Share Dialog Social Media")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Share Dialog Social Media test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


    def test_course_f_start_referring_button(self):
        print("\nStarted testing referring button")
        try:
            # Navigate to an paid course
            self.driver.get(unerolled_course_url)

            # Locate the Start Referring button
            start_referring_button_path = '//*[@id="root"]/div[8]/div/div[8]/div/div[1]/div/button/a'
            
            # Wait for the Start Referring button to become clickable
            start_referring_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, start_referring_button_path)))

            # Scroll the Start Referring button into view
            self.driver.execute_script("arguments[0].scrollIntoView();", start_referring_button)

            # Click the Start Referring button using JavaScript
            self.driver.execute_script("arguments[0].click();", start_referring_button)

            # Assertion to check if the desired page is loaded
            assert "refer-a-friend" in self.driver.current_url

            print("Successfully tested start referring button")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Start referring button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
        
    
    
    
if __name__ == '__main__':
    unittest.main()
        