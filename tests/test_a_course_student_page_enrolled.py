import unittest
import logging
import time
import json
import sys
import string
import random
import traceback
import pyautogui

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url
from helpers import wait_for_pageload, create_driver, login
from selenium.webdriver.common.keys import Keys
from data.test_data import valid_coupon_code, invalid_coupon_code, existing_module_url
from data.test_data import test_file_path

# Configure logging to write to a file
logging.basicConfig(filename='error_for_a_course_student_page.log',level=logging.ERROR)

class CourseEnrolledStudentPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)

        # Check if the role of the user is "teacher"
        if self.course_page.get_user_role() == "T":
            # Switch the role to "teacher"
            self.course_page.switch_role()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()
    
    @classmethod
    def setUp(self):
        time.sleep(2)

    def test_enroll_now_button_with_enough_keys(self):
        print("\nStarted testing enroll now button for an unpaid course with enough keys")
        try:
            # Navigate to an unpaid course
            self.course_page.navigate_to_a_unpaid_course()

            time.sleep(3)
            
            # Find and click help button
            admin_menu_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div')))
            admin_menu_button.click()

            unenroll_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[1]/div')))
            unenroll_button.click()

            confirmation_popup = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                    )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(confirmation_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            enrollment_reverted_confirmation = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                    )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(enrollment_reverted_confirmation, 20).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[3]/div[2]/button'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            # Locate the enroll now button on the course page
            enroll_now_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[1]/button[1]')))
            
            # Click the enroll now button
            enroll_now_button.click()

            # Wait for the element with the description text to be visible
            wait = WebDriverWait(self.driver, 10)

            # Check if the description element is visible (enrollment success)
            description_element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/p')))
            
            # Check if the description text contains the desired phrase
            desired_phrase = "you have been successfully enrolled in the course."
            text_contains_desired_phrase = desired_phrase in description_element.text

            # Perform assertions or further actions based on the result
            assert text_contains_desired_phrase, "Enrollment success message not found"

            print("Successfully tested the enroll now button for an unpaid course with enough keys")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Enroll now button withe enough keys test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_enroll_now_button_for_paid_course(self):
        print("\nStarted testing enroll now button for a paid course")
        
        try:
            # Navigate to an unpaid course
            self.course_page.navigate_to_a_paid_course()

            time.sleep(3)
            
            # Find and click help button
            admin_menu_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div')))
            admin_menu_button.click()

            unenroll_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div[1]/div')))
            unenroll_button.click()

            confirmation_popup = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                    )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(confirmation_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            enrollment_reverted_confirmation = WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                    )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(enrollment_reverted_confirmation, 20).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[3]/div[2]/button'))
            )
            
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            # Click the "Apply Coupon" button
            apply_coupon_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/div/button')))
            self.driver.execute_script("arguments[0].click();", apply_coupon_button)
            
            # Enter the coupon code in the input field
            coupon_input_field = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/input')))
            coupon_input_field.clear()  # Clear any existing value in the input field
            coupon_input_field.send_keys('lfp-test-100')  # Enter your desired coupon code
            
            # Submit the coupon
            coupon_submit_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/button')))
            coupon_submit_button.click()
            
            # Verify if the coupon was successfully applied
            successfully_applied_text = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/form/p[1]'))).text

            expected_text = "Coupon Applied"
            assert successfully_applied_text == expected_text, f"Element text '{successfully_applied_text}' does not match expected text '{expected_text}'"


            # Locate the enroll now button on the course page
            enroll_now_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[8]/div/div[1]/div[3]/div[2]/div/div[2]/div[1]/button[1]')))
            
            # Click the enroll now button
            enroll_now_button.click()

            # Wait for the element with the description text to be visible
            wait = WebDriverWait(self.driver, 10)

            # Check if the description element is visible (enrollment success)
            description_element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div/div[1]/p')))
            
            # Check if the description text contains the desired phrase
            desired_phrase = "you have been successfully enrolled in the course."
            text_contains_desired_phrase = desired_phrase in description_element.text

            # Perform assertions or further actions based on the result
            assert text_contains_desired_phrase, "Enrollment success message not found"

            print("Successfully tested the enroll now button for a paid course")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Enroll now button for a paid course test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_video(self):
        print("\nStarted testing the Module Content-Video button in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the video content button
            video_content = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="Learn+JavaScript+by+Building+7+Games+-+Full+Course26a7d"]/div/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", video_content)

            time.sleep(5)

            #Find the current url of the page
            current_url = self.driver.current_url

            # Check if the video identifier is present in the URL
            assert 'video' in current_url, "The URL does not contain the keyword 'video'"

            print("Successfully tested the Module Content-Video button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Video button in Student Mode test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_pdf_reading_material(self):
        print("\nStarted testing the Module Content- PDF Reading material button in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the pdf reading material button
            pdf_reading_material = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div/div[2]/div/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", pdf_reading_material)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("External PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("External PDF not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-PDF Reading material button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-PDF Reading material in Student Mode button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_external_link_reading_material(self):
        print("\nStarted testing the Module Content- External link Reading material button")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the external link reading material button
            external_link_reading_material = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="title-not-found-9f56f"]/div/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", external_link_reading_material)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("External PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("External PDF not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-External link Reading material button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-External link Reading material button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_time_for_a_quiz(self):
        print("\nStarted testing the Module Content-Time for a quiz! button in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            #Find the 'Time for a quiz' button
            time_for_a_quiz_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div/div[3]/div/div/a')))
            
            #Click the video content button
            self.driver.execute_script("arguments[0].click();", time_for_a_quiz_button)
            
            #Find the lets go button
            lets_go_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
            
            #Click on the lets go button
            lets_go_button.click()

            # Switch to the new window or tab opened for the PDF
            window_handles = self.driver.window_handles
            if len(window_handles) > 1:
                # Switch to the new window
                self.driver.switch_to.window(window_handles[1])
                # Check if the page title or URL indicates a PDF
                if "pdf" in self.driver.current_url.lower():
                    print("Quiz PDF opened successfully")
                    assert True  # Assertion for successful PDF opening
                else:
                    print("Quiz Pdf not found")
                    assert False  # Assertion for unsuccessful PDF opening
                # Close the PDF window
                self.driver.close()
                # Switch back to the main window
                self.driver.switch_to.window(window_handles[0])
            else:
                print("No new window or tab opened for the PDF")
                assert False  # Assertion for no new window or tab opened

            print("Successfully tested the Module Content-Time for a Quiz button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content- Time for a quiz button in Student Mode test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_demo_project_add_link(self):
        print("\nStarted testing the Module Content-Demo project add link in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div/div[5]/div/div/p/span'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Check if the projects dialog is displayed
            projects_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert projects_dialog.is_displayed(), "The projects dialog is not displayed"

            # Find and click the 'Submission' button
            submission_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/button'))
            )
            submission_button.click()

            # Check if the Select Project Dialog is displayed
            select_project_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert select_project_dialog.is_displayed(), "The Select Project dialog is not displayed"

            add_link_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div[2]/span'))
            )
            add_link_button.click()

            link_input_field = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="common-dialog-description"]/div/div/input')))
            link_input_field.send_keys("https://beltline.org/wp-content/uploads/2020/09/placeholder-pdf.pdf")

            submit_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[4]/div[3]/div/div[3]/button'))
            )
            submit_button.click()

            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'notistack-CollapseWrapper'))
            )
            
            assert success_message.text == "Link Added Successfully"
            
            print("Successfully tested the Module Content-Demo Project Add link button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Project Add link button in Student Mode test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_student_module_content_demo_project_upload_file(self):
        print("\nStarted testing the Module Content-Demo project Upload file in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Project' button
            demo_project = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div/div[5]/div/div/p/span'))
            )
            self.driver.execute_script("arguments[0].click();", demo_project)

            # Check if the projects dialog is displayed
            projects_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert projects_dialog.is_displayed(), "The projects dialog is not displayed"

            # Find and click the 'Submission' button
            submission_button = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/button'))
            )
            submission_button.click()

            # Check if the Select Project Dialog is displayed
            select_project_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert select_project_dialog.is_displayed(), "The Select Project dialog is not displayed"

            file_upload_input = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="common-dialog-description"]/div[2]/label/span')))
            
            # Click the file upload button
            file_upload_input.click()

            # Wait for the file selection dialog to appear
            pyautogui.sleep(1)

            # Set the file path to be uploaded
            file_path = path = test_file_path

            # Type the file path in the file selection dialog
            pyautogui.write(file_path)

            # Press Enter to confirm the file selection
            pyautogui.press("enter")
            
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'notistack-snackbar'))
            )

            assert success_message.is_displayed()            
            print("Successfully tested the Module Content-Demo Project Upload file button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Project Upload file button in Student Mode test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_demo_question_add_answer(self):
        print("\nStarted testing the Module Content-Demo question Add Answer button in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="<p>demo-questio-496c8"]/div/div/p'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'Add Answer' button
            add_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[2]/span/button'))
            )
            add_answer_button.click()

            # Locate the div element
            answer_textbox = self.driver.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div')

            # Interact with the div element (e.g., clear the content)
            answer_textbox.send_keys(Keys.CONTROL + "a")
            answer_textbox.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "This is my answer" + suffix

            # Enter text into the div element
            answer_textbox.send_keys(text_to_enter)

            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary'))
            )
            self.driver.execute_script("arguments[0].click();", submit_button)

            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-text'))
            )

            assert success_message.text == "Answer submitted successfully"
                        
            print("Successfully tested the Module Content-Demo Question Add answer button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Question Add answer in Student Mode test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_student_module_content_demo_question_view_answer(self):
        print("\nStarted testing the Module Content-Demo question View Answer button in Student Mode")

        try:
            # Navigate to the module
            self.driver.get(existing_module_url)

            # Find the module element
            module = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.ID, 'module_6'))
            )

            # Find and click the 'Demo Question' button
            demo_question = WebDriverWait(module, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="41033"]/div/div[6]/div/div/p/span'))
            )
            self.driver.execute_script("arguments[0].click();", demo_question)

            # Check if the "Answer a question" dialog is displayed
            answer_a_question_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert answer_a_question_dialog.is_displayed(), "The Answer a question dialog is not displayed"

            # Find and click the 'View Answer' button
            view_answer_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/span[1]/span/button'))
            )
            view_answer_button.click()

            # Check if the All Answers dialog is displayed
            all_answers_dialog = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )
            assert all_answers_dialog.is_displayed(), "The All answers dialog is not displayed"

            close_all_answer_dialog_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-title"]/button/span[1]'))
            )
            self.driver.execute_script("arguments[0].click();", close_all_answer_dialog_button)

                        
            print("Successfully tested the Module Content-Demo Question View Answer button in Student Mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Module Content-Demo Question View Answer in Student Mode test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

if __name__ == '__main__':
    unittest.main()
        
        