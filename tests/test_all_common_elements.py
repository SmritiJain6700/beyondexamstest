import unittest
import logging
import time
import json
import sys

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from selenium.webdriver.common.keys import Keys
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url, home_page_url
from helpers import wait_for_pageload, create_driver, login
from data.test_data import valid_coupon_code, invalid_coupon_code

# Configure logging to write to a file
logging.basicConfig(filename='error_for_common_elements.log', level=logging.ERROR)

class CommonElementsTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)
    
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    @classmethod
    def setUp(self):
        time.sleep(2)
        self.driver.get(home_page_url)
        
    def test_presence_of_navbar(self):
        print("\nStarted testing the presence of the navbar")

        try:
            # Locate the navbar element
            navbar = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]')

            # Verify if the navbar is displayed
            self.assertTrue(navbar.is_displayed(), "Navbar is not present or visible on the page.")
            
            print("Successfully tested the presence of the navbar")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Presence of navbar test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_presence_of_search_bar(self):
        print("\nStarted testing presence of the search bar")
    
        try:
            # Verify the presence of the search bar within the navbar
            search_bar = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[1]')
            self.assertTrue(search_bar.is_displayed(), "Search bar is not present or visible within the navbar.")
            
            print("Successfully tested the presence of the Search bar")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Presence of search bar test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
            
    def test_search_bar_valid_input(self):
        print("\nStarted testing search bar with valid input")
    
        search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
        search_bar.click()
        
        try:
            # Find the search input element and enter an invalid search term 'computer'
            search_input = self.driver.find_element(By.XPATH, '//*[@id="nav-search"]')
            search_input.send_keys('computer')
            search_input.send_keys(Keys.RETURN)
            
            # Wait for the presence of search results
            results_displayed = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'search_mid'))
            ).is_displayed()
            
            self.assertTrue(results_displayed, "Search results not displayed for valid input.")
            
            print("Successfully tested the search bar providing search results successfully for valid input.")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Search bar valid input test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_top_searches_displayed(self):
        print("\nStarted testing if top searches are displayed")
        
        search_bar = self.driver.find_element(By.CLASS_NAME, 'Nav-search')
        search_bar.click()
        try:
            # Wait for the 'top_search' element to become visible
            wait = WebDriverWait(self.driver, 10)
            top_search = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[4]/p')))
            
            # Check if the 'top_search' element is displayed on the page
            self.assertTrue(top_search.is_displayed(), "Top search is not displayed")
            
            print("Successfully tested if top searches are displayed")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Top searches displayed test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_our_services_dropdown(self):
        print("\nStarted testing our services dropdown button working.")
        
        try:
            # Locate the our serivces button
            our_services_element =  self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div/div[2]/div')
            
            # Check if the button is displayed and enabled
            self.assertTrue(our_services_element.is_displayed(), "Our Services button is not displayed.")
            self.assertTrue(our_services_element.is_enabled(), "Our Services button is not enabled.")
            
            # Create an instance of ActionChains
            actions = ActionChains(self.driver)

            # Move the mouse to the target element
            actions.move_to_element(our_services_element)

            # Perform an additional action to trigger the dropdown
            actions.click().perform()

            # Wait for the dropdown to become visible
            dropdown = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, "DesktopProfileViewer")))
            
            # Find all the dropdown options
            dropdown_options = self.driver.find_element(By.CSS_SELECTOR, "a")
            
            expected_options = ["Learn a skill", "Revise your syllabus", "Create a course", "YT playlist to BE course", "YT video to BE video", "Homeschool your kids", "Learn a language"]
            
            
            #  Verify the presence of expected options in the dropdown
            for option in expected_options:
                assert option in expected_options, f"Expected option '{option}' not found in the dropdown options."
            
            print("Successfully tested that serives dropdown button is working properly.")
        
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Our services dropdown test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    
    def test_courses_link(self):
        print("\nStarted testing the 'Courses' link in the navigation bar")
    
        try:
            # Find the 'Courses' link element
            courses_link = self.driver.find_element(By.XPATH, "//a[@href='/learn-a-skill']")
            
            # Check if the link is displayed and enabled
            self.assertTrue(courses_link.is_displayed(), "'Courses' link is not displayed on the page.")
            self.assertTrue(courses_link.is_enabled(), "'Courses' link is not enabled.")
            
            # Click on the link
            courses_link.click()
            
            # Check if the page has navigated to the expected URL
            expected_url = learn_a_skill_url  # Replace with the actual expected URL
            self.assertEqual(self.driver.current_url, expected_url, "Navigation to 'Courses' page failed.")
            
            print("Successfully tested 'Courses' link in the navigation bar is working properly.")
        
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Courses link test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_logo_redirection(self):
        print("\nStarted testing logo redirection to the homepage.")

        try:
            # Find the logo element
            logo_element = self.driver.find_element(By.CLASS_NAME, 'nav-logo')

            # Verify the presence of the logo element
            self.assertTrue(logo_element.is_displayed(), "Logo element is not present or visible on the page.")

            # Click on the logo element
            logo_element.click()

            # Verify that the page has been redirected to the homepage
            expected_homepage_url = home_page_url  # Replace with the expected homepage URL
            self.assertEqual(self.driver.current_url, expected_homepage_url, "Logo redirection to the homepage failed.")
            
            print("Successfully tested logo redirection to the homepage.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Logo redirection test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_help_button(self):
        print("\nStarted testing the help button")

        try:

            # Find the element using XPath
            help_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div')))

            # Check if the button is displayed and enabled
            self.assertTrue(help_button.is_displayed(), "Help button is not displayed.")
            self.assertTrue(help_button.is_enabled(), "Help button is not enabled.")
            
            # Click on the button
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )

            # Find the child elements within the parent element
            help_options = viewer_element.find_elements(By.CSS_SELECTOR, ".DesktopProfileBtn")

            # Get the text of each dropdown option
            help_options_texts = [option.text for option in help_options]

            expected_options = ["Chat with us", "Send feedback", "Report a problem", "Watch Tutorial"]

            # Print the text of each child element
            for option in expected_options:
                assert option in help_options_texts, f"Expected option '{option}' not found in the help options."
            
            print("Successfully tested the help button")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
        
    def test_help_button_chat_with_us(self):
        print("\nStarted testing the help button - Chat with us")

        try:
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            chat_with_us_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/a/div'))
            )
            chat_with_us_button.click()

            # Verify the URL contains "support" indicating the chat window is open
            self.assertIn("Support", self.driver.current_url)

            # Verify the chat window header text
            chat_header = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[2]/div[1]/div[1]/div[1]/h4'))
            )
            self.assertEqual(chat_header.text, "Beyondexams Support")

            print("Successfully tested help button - Chat with us")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Chat with us test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
                
    
    def test_help_button_submit_feedback(self):
        print("\nStarted testing the help button - Submit Feedback")

        try:
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            submit_feedback_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[1]/div'))
            )
            submit_feedback_button.click()

            feedback_form =  WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
                )
            
            feedback_input = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH,'//*[@id="feedback"]')))
            feedback_input.clear()  # Clear any existing value
            feedback_input.send_keys("This is a test feedback")  # Enter the test feedback
            
            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/button')))
            
            submit_button.click()
            
            # Verify if the expected error message is displayed
            success_message = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[3]'))).text

            expected_message = "Feedback added successfully"
            assert success_message == expected_message, f"Element text '{success_message}' does not match expected text '{expected_message}'"

            print("Successfully tested the Submit feedback functionality")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Submit Feedback test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
                
    def test_help_button_report_a_problem(self):
        print("\nStarted testing help button - Report a problem")
        
        try:
            
            # Find the help button using XPath
            help_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div'))
            )
            help_button.click()

            # Wait for the viewer to appear
            viewer_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[2]/div[1]'))
            )
            
            # Wait for the chat with us button to appear
            report_a_problem_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div[1]/div[2]'))
            )
            report_a_problem_button.click()

            report_form = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div'))
            )

            self.assertTrue(report_form.is_displayed())
    
            print("Successfully tested help button - Report a problem")
    
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Help button - Report a problem test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    

    def test_profile_button_hover_menu_display(self):
        print("\nStarted testing hovering profile button displays menu")
        
        try:
            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')
            
            # Check if the profile button is displayed
            self.assertTrue(profile_button.is_displayed(), "Explore Services button is not displayed.")
            
            #Simulate a mouseover event using JavaScript
            self.driver.execute_script("arguments[0].dispatchEvent(new Event('mouseover', { bubbles: true }));", profile_button)

            # Wait for the sidebar to become visible
            sidebar = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]')))
            self.assertTrue(sidebar.is_displayed(), "The element is not displayed.")
            
            print("Successfully tested menu displayed on hovering over profile button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Profile button displays menu test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_profile_role_circle_display(self):
        print("\nStarted testing the profile role circle display")
        try:
            # Find the role circle element
            role_circle = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div/div')))

            # Verify the display based on the user's role
            role = "teacher"  # Replace with the actual user role ("teacher" or "student")
            if role == "teacher":
                self.assertEqual(role_circle.text, "T")
            else:
                self.assertEqual(role_circle.text, "S")
            
            print("Successfully tested the profile role circle display")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Profile role circle display test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_profile_menu(self):
        print("\nStarted testing the profile menu")
        try:
            
            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Simulate a mouseover event using JavaScript
            self.driver.execute_script("arguments[0].dispatchEvent(new Event('mouseover', { bubbles: true }));", profile_button)

            # Wait for the sidebar to become visible
            sidebar = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]')))

            # Find the nav profile element
            nav_profile = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/div[1]')))

            # Verify the display and background color based on the user's role
            role = "teacher"  # Replace with the actual user role ("teacher" or "student")
            if role == "teacher":
                self.assertEqual(nav_profile.text, "Teacher")
                self.assertEqual(nav_profile.value_of_css_property("background-color"), "rgba(255, 0, 0, 1)")  
            else:
                self.assertEqual(nav_profile.text, "Student")
                self.assertEqual(nav_profile.value_of_css_property("background-color"), "rgb(102, 70, 231)")  

            # Get the <a> tags from the sidebar
            menu_options = sidebar.find_elements(By.TAG_NAME, 'a')
            
            # Get the text of each menu option
            menu_option_texts = [option.text.replace("\n", " ") for option in menu_options]

            # Expected menu options
            expected_menu_options = ['Go to your dashboard', 'Keys available find out what they unlock', 'Refer A Friend', 'Video History', 'Create a course', 'Get involved']
            
            # Perform assertions or further actions with the menu options
            for option in range(0,len(menu_options)):
                assert expected_menu_options[option] in menu_option_texts[option], f"Expected option '{menu_option_texts[option]}' not found in the menu options."
        
            print("Successfully tested the profile menu")
        
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Profile menu test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_go_to_your_dashboard_button(self):
        print("\nStarted testing Go to your dashboard button")

        try:

            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            # Find the go to your dashboard button
            go_to_your_dashboard_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[1]')))

            # Click the go to your dashboard button using JavaScript
            self.driver.execute_script("arguments[0].click();", go_to_your_dashboard_button)

            # Get the current URL
            current_url = self.driver.current_url

            # Assert that the URL contains the "profile" keyword
            self.assertIn("profile", current_url, "URL does not contain 'profile'")

            # Check the profile name
            check_profile_name = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="profile"]/div[6]/div[1]/div[1]/div[2]/h2'))).text

            # Assert that the profile name matches the expected name
            self.assertEqual(check_profile_name, settings['user_name'], "Profile name does not match")

            print("Successfully tested the go to your dashboard button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Go to your dashboard button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_find_out_what_keys_unlock(self):
        print("\nStarted testing find out what keys unlock button")

        try:

            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            # Locate the 'find_what_keys_unlock' button
            find_what_keys_unlock = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[2]')))

            # Click the go to your dashboard button using JavaScript
            self.driver.execute_script("arguments[0].click();", find_what_keys_unlock)

            # Switch to the new tab
            self.driver.switch_to.window(self.driver.window_handles[1])

            # Get the current URL
            current_url = self.driver.current_url

            # Assert that the URL contains the "profile" keyword
            self.assertIn("Keys", current_url, "URL does not contain 'Keys'")

            # Close the new window or tab
            self.driver.close()

            # Switch back to the original window or tab
            self.driver.switch_to.window(self.driver.window_handles[0])

            print("Successfully tested the 'find_what_keys_unlock_button'")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Find out what keys unlock button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_refer_a_friend_button(self):
        print("\nStarted testing refer a friend button")

        try:

            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            # Locate the 'refer_a_friend_button' button
            refer_a_friend_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[3]')))

            # Click the go to your dashboard button using JavaScript
            self.driver.execute_script("arguments[0].click();", refer_a_friend_button)

            # Get the current URL
            current_url = self.driver.current_url

            # Assert that the URL contains the "refer-a-friend"
            self.assertIn("refer-a-friend", current_url, "URL does not contain 'refer-a-friend'")
    
            # Get the referral heading element and remove line breaks
            referral_heading_element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div[1]/div[2]/h1')))
            referral_heading = referral_heading_element.text.replace('\n', ' ')

            # Assert the referral heading text
            self.assertEqual("INVITE FRIENDS AND LEARN TOGETHER", referral_heading)

            print("Successfully tested the 'refer_a_friend_button'")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Refer a friend button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_switch_user_role_button(self):
        print("\nStarted testing the 'switch user role' button")

        try:
            initial_profile_role = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div/div/div')

            # Get the initial role of the user
            initial_role_of_user = initial_profile_role.text

            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            switch_user_role_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/div[5]')))

            # Click the "Switch Role" button
            switch_user_role_button.click()

            switch_user_role_confirmation = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))

            self.driver.execute_script("arguments[0].click();", switch_user_role_confirmation)

            profile_role_after_switch = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,'//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div/div/div')))

            # Get the user role after switching
            user_role_after_switch = profile_role_after_switch.text

            # Assert that the user role has changed
            self.assertNotEqual(initial_role_of_user, user_role_after_switch, "User role was not switched successfully")

            print("Successfully tested the 'switch user role' button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Switch user role button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_video_history_button(self):
        print("\nStarted testing the 'video history' button")

        try:
        
            # Find the profile button element
            profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            video_history = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[4]')))

            # Click the "video history" button
            video_history.click()

            # Get the current URL
            current_url = self.driver.current_url

            # Assert that the URL contains the "history"
            self.assertIn("history", current_url, "URL does not contain 'history'")
    
            # Get the history_heading 
            history_heading = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div/div[1]/h1'))).text

            # Assert the referral heading text
            self.assertEqual("Watch History", history_heading)

            print("Successfully tested the 'video_history_button'")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Video history button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_create_course_button_menu_student(self):
        print("\nStarted testing the create course button in the menu - Student mode")

        try:
            # Check if the user role is already "student"
            if self.course_page.get_user_role() != "S":
                # Switch user role to "student"
                self.course_page.switch_role()  # Assuming a method to switch user role is available

            # Find the profile button element
            profile_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,'//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')))

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            # Check if the create course button is accessible to students
            create_course_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[5]')))
            create_course_button.click()

            logged_in_as_student_popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div[2]')))
            
            popup_text = logged_in_as_student_popup.text
            expected_text = "Logged in as a student"
            assert popup_text == expected_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}"

            print("Successfully tested the create course button in the menu - Student mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Create course button in the menu - Student mode test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_create_course_button_menu_teacher(self):
        print("\nStarted testing the create course button in the menu - Teacher mode")

        try:
            # Check if the user role is already "teacher"
            if self.course_page.get_user_role() != "T":
                # Switch user role to "teacher"
                self.course_page.switch_role()  # Assuming a method to switch user role is available

            # Find the profile button element
            profile_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH,'//*[@id="root"]/div[1]/div[5]/div[2]/div[1]')))

            # Perform a mouseover action using ActionChains
            actions = ActionChains(self.driver)
            actions.move_to_element(profile_button).perform()

            # Check if the create course button is accessible to teachers
            create_course_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/a[5]')))
            create_course_button.click()

            create_course_confirmation = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[3]/div/div[3]/button[1]')))
            
            create_course_confirmation.click()

            # Wait for the popup to appear
            popup_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[4]/div/div[1]')))

            # Verify the content of the popup
            popup_text = popup_element.text
            expected_text = "Click here to go to courses section"
            
            self.assertIn(expected_text, popup_text, f"Popup text mismatch. Expected: {expected_text}, Actual: {popup_text}")

            print("Successfully tested that create course button in the menu - Teacher mode")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Create course button menu - teacher mode test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_explore_all_resources_button(self):
        print("\nStarted testing the 'Explore All Resources' button")

        try:
            # Find the element using CSS selector
            explore_services_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[15]/div[1]/div/div'))
            )
            
            # Check if the button is displayed and enabled
            self.assertTrue(explore_services_button.is_displayed(), "Explore Services button is not displayed.")
            self.assertTrue(explore_services_button.is_enabled(), "Explore Services button is not enabled.")

            # Click on the button
            explore_services_button.click()

            # Wait for the content to expand
            content_element = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[15]/div[1]/div/div/div[2]'))
            )

            # Verify if the content is expanded
            is_expanded = content_element.is_displayed()

            # Assert the content is expanded
            self.assertTrue(is_expanded, "The 'Explore All Resources' content is not expanded.")
    
            print("Successfully tested the 'Explore All Resources' button")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Explore all resources button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
    
    def test_copyright_section(self):
        print("\nStarted testing the copyright section")

        try:
            # Find the copyright section element using appropriate selector or XPath
            copyright_section = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[15]/div[3]')))

            # Scroll the element into view
            self.driver.execute_script("arguments[0].scrollIntoView();", copyright_section)

            # Verify that the copyright section is displayed
            self.assertTrue(copyright_section.is_displayed(), "Copyright section is not displayed")

            # Verify the content accuracy
            expected_text = f"Copyright © 2023, Beyondexams. All Rights Reserved"
            actual_text = copyright_section.find_element(By.XPATH, '//*[@id="root"]/div[15]/div[3]/div/h4[1]').text
            self.assertEqual(expected_text, actual_text, "Incorrect copyright statement")

            # Verify the funded by text and link
            funded_by_text = "Funded by Dr.Aniruddha Malpani"
            funded_by_element = copyright_section.find_element(By.XPATH, '//*[@id="root"]/div[15]/div[3]/div/h4[2]')
            self.assertTrue(funded_by_text in funded_by_element.text, "Funded by text is incorrect")

            funded_by_link = funded_by_element.find_element(By.XPATH, '//*[@id="root"]/div[15]/div[3]/div/h4[2]/a')
            self.assertTrue(funded_by_link.is_displayed(), "Funded by link is not displayed")
            self.assertEqual(funded_by_link.get_attribute("href"),"https://twitter.com/malpani","Incorrect link",)

            print("Successfully tested the copyright section")
            
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "copyright section test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            
    def test_footer_presence(self):
        print("\nStarted testing the presence of the footer")
        try:
            # Find the footer element
            footer = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[15]/div[2]/div')

            # Verify the presence of the footer
            self.assertTrue(footer.is_displayed(), "Footer is not present on the page.")

            print("Successfully tested the presence of the footer")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Footer presence test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_social_media_icons_footer(self):
        print("\nStarted testing social media icons in the footer ")
        try:
            # Find the social media icons container element
            social_media_icons = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[15]/div[2]/div/div[2]/div[1]/div/div')

            # Find all the social media icons within the container
            icons = social_media_icons.find_elements(By.TAG_NAME, 'a')

            # Define the expected social media URLs
            expected_urls = [
                "https://www.instagram.com",
                "https://www.facebook.com",
                "https://www.linkedin.com",
                "https://twitter.com"
            ]

            # Click on each social media icon and verify the URL
            for i, icon in enumerate(icons):
                # Get the link URL from the icon's parent anchor element
                link_url = icon.get_attribute('href')

                # Open the link in a new tab/window
                self.driver.execute_script("window.open('');")
                self.driver.switch_to.window(self.driver.window_handles[1])
                self.driver.get(link_url)

                # Verify if the new tab/window is opened successfully
                self.assertIn(expected_urls[i], self.driver.current_url, "Failed to open social media link.")
                # Close the new tab/window
                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[0])
                
            print("Successfully tested the functionality of footer social media icons")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Social media icons footer test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)
            

if __name__ == '__main__':
    unittest.main()
        
        