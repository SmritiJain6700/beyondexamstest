import unittest
import json
import sys
import time


with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])
 
from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.login_page import LoginPage
from helpers import wait_for_pageload, create_driver, login
from selenium.webdriver.common.action_chains import ActionChains


class LoginTest(unittest.TestCase):
    
    driver = None

    @classmethod
    def setUp(inst):
        inst.driver = create_driver()
        inst.logged_in = False

    @classmethod
    def tearDown(inst):
        # close the browser window
        inst.driver.quit()

    def login(self):
        self.logged_in = login(self.driver)
        
    def test_login(self):
        self.login()               
        
             
if __name__ == '__main__':
    unittest.main()
