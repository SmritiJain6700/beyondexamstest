import unittest
import logging
import time
import json
import sys

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from helpers import wait_for_pageload, create_driver
from data.urls import referral_code, learn_a_skill_url, revise_your_syllabus_url

# Configure logging to write to a file
logging.basicConfig(filename='error_courses_page.log', level=logging.ERROR)

class CoursesPageTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)
    
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
            
    def setUp(self):
        # Set up instance-level preconditions for each test
        self.navigate_to_course_page()
        
    def navigate_to_course_page(self):
        #return to homepage
        self.course_page.load()
        
    def find_and_click_more_options_button(self):
        # Check if the role of the user is "teacher"
        if self.course_page.get_user_role() == "T":
            # Switch the role to "student"
            self.course_page.switch_role()
            
        more_options = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]/div[2]/div')))
        more_options.click()
        
    def test_learn_skill_button(self):
        print("\nStarted testing Learn a Skill button")

        try:
            # Find and click the 'Learn a skill' button
            learn_skill_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[1]/div/button[1]')))
            learn_skill_button.click()
            current_url = self.driver.current_url

            print("Learn a skill button found and clickable")
            
            self.assertEqual(current_url, learn_a_skill_url)
            
            # Wait for the 'courses_displayed' element to become visible
            wait = WebDriverWait(self.driver, 10)
            courses_displayed = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]')))

            # Check if the 'courses_displayed' element is found on the page
            courses_displayed = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]')
            self.assertTrue(courses_displayed, "Courses are not displayed under Learn a skill")

            print("Courses are displayed under learn a skill")

            print("Test case passed! Learn a skill button is working properly")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Learn skill button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

            
    def test_revise_syllabus_button(self):
        print("\nStarted testing Revise Your Syllabus button")

        try:
            # Find and click the 'Revise Your Syllabus' button
            revise_syllabus_button = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[1]/div/button[2]')))
            revise_syllabus_button.click()
            
            current_url = self.driver.current_url

            print("Revise Your Syllabus button found and clicked")

            # Verify if the current URL matches the expected URL
            self.assertEqual(current_url, revise_your_syllabus_url)
            
            # Wait for the 'courses_displayed' element to become visible
            wait = WebDriverWait(self.driver, 10)
            courses_displayed = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]')))

            # Check if the 'courses_displayed' element is found on the page
            self.assertTrue(courses_displayed, "Courses are not displayed under Revise Your Syllabus")

            print("Courses are displayed under revise your syllabus")

            print("Test case passed! Revise Your Syllabus button is working properly")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Revise syllabus button test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_course_more_options(self):
        print("\nStarted testing Course More Options")

        try:
            self.find_and_click_more_options_button()

            # Wait for the more options to be visible
            more_options = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]/div[2]/div/div[2]/div/ul/li')))

            # Verify that the more options contain "Share" and "Report"
            expected_options = ["Share", "Report"]
            more_option_texts = [option.text for option in more_options]

            # Verify the presence of expected options in the more options
            for option in expected_options:
                assert option in more_option_texts, f"Expected option '{option}' not found in the more options."

            print("Test case passed! Courses More Options working successfully.")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Course more options test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_course_share_link_copied_successfully(self):
        print("\nStarted testing Course Share Link Generation")

        try:
            self.find_and_click_more_options_button()

            # Click the "Share" button
            share_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]/div[2]/div/div[2]/div/ul/li[1]')))
            share_button.click()

            # Click copy link button
            copy_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div')))
            copy_button.click()

            success_message = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))).text

            # Verify if the copied link matches the generated link
            print(success_message)
            assert success_message == "Link Copied Successfully"

            print("Successfully copied the course share link")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Course share link copied successfully test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_share_dialog_social_media(self):
        print("\nStarted testing Share Dialog Social Media")

        try:
            self.find_and_click_more_options_button()

            # Click the "Share" button
            share_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]/div[2]/div/div[2]/div/ul/li[1]')))
            share_button.click()

            # Wait for the dialog to open
            dialog = self.driver.find_element(By.XPATH, '/html/body/div[2]/div[3]/div')

            # Verify the working of social media platform icons
            social_media_icons = dialog.find_elements (By.XPATH, '//*[@id="common-dialog-description"]/div[1]/button')
            expected_icons = {
                'facebook': 'facebook.com',
                'linkedin': 'linkedin.com',
                'twitter': 'twitter.com',
                'instagram': 'instagram.com',
                'whatsapp': 'whatsapp.com'
            }

            # Click each social media platform icon and validate the behavior
            for icon in social_media_icons:
                icon_name = icon.get_attribute('aria-label')
                icon.click()

                window_handles = self.driver.window_handles

                # Switch to the new window or tab
                self.driver.switch_to.window(window_handles[-1])

                # Validate the expected behavior for each social media platform
                if icon_name in expected_icons:
                    expected_url = expected_icons[icon_name]
                    assert expected_url in self.driver.current_url, f"{icon_name.capitalize()} did not open in a new tab."

                # Close the new window or tab
                self.driver.close()

                # Switch back to the original window or tab
                self.driver.switch_to.window(window_handles[0])

            print("Successfully tested Share Dialog Social Media")
        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Share Dialog social media test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_course_reported_successfully(self):
        print("\nStarted testing Course reported")

        try:
            self.find_and_click_more_options_button()

            # Click the "Report" button
            report_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[5]/div/div[3]/div[2]/div/div/div/a[1]/div[2]/div/div[2]/div/ul/li[2]')
            report_button.click()

            # Wait for the "Course Reported" popup to appear
            popup = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Assert that the "Course Reported" popup is displayed
            print(popup.text)
            assert popup.text == "Course Reported", "Course Reported popup did not appear."
            
            print("Successfully tested if course reported")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Course reported successfully test failed."

            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


if __name__ == '__main__':
    unittest.main()
        