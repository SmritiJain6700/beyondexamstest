import unittest
import logging
import time
import json
import sys
import string
import random
import traceback
import pyautogui
from selenium.webdriver.common.action_chains import ActionChains

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

from selenium import webdriver
from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.common import TimeoutException, NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.course_page import CoursePage
from helpers import wait_for_pageload, create_driver, login
from selenium.webdriver.common.keys import Keys
from data.test_data import test_file_path, testing_pdf, video_page_url


# Configure logging to write to a file
logging.basicConfig(filename='error_video_page_for_teacher.log',level=logging.ERROR)

class VideoPageTeacherTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = create_driver()
        self.course_page = CoursePage(self.driver)

        # Check if the role of the user is "student"
        if self.course_page.get_user_role() == "S":
            print(self.course_page.get_user_role())
            # Switch the role to "TEACHER
            self.course_page.switch_role()
        

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def setUp(self):
        time.sleep(2)
        # Set up instance-level preconditions for each test
        self.navigate_to_video_page()
                
    def navigate_to_video_page(self):
        # Navigate to the Video
        self.driver.get(video_page_url)


    def test_notes_button(self):
        print("\nStarted testing the Notes - Add, Edit, Share, Delete")
        try:
            #----------Testing Add Notes Feature----------

            # Using CSS selector with data-tut attribute
            notes_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '[data-tut="notes"]'))
            )

            # Click the 'Notes' button
            self.driver.execute_script("arguments[0].click();", notes_button)

            #Add notes 
            add_notes_input = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[1]/form/div/input')))  
            
            self.driver.execute_script("arguments[0].click();", add_notes_input)

            text_to_enter = "Test Note"
            # Select all the existing text and delete it
            add_notes_input.send_keys(text_to_enter)
            
            add_notes_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-1"]/div/div[1]/form/button')))  
            
            self.driver.execute_script("arguments[0].click();", add_notes_button)

            notification =  WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notificationBox"]'))
            )

            # Get the text of the notification
            notification_text = notification.text

            # Assert the expected text
            expected_text = "You got 10 bonus keys for creating notes!"
            assert  expected_text in notification_text, f"Expected text: '{expected_text}', Actual text: '{notification_text}'"

            print("\nSuccessfully tested the Add Notes Feature")

            #----------Testing Edit Notes Feature----------

            #Using CSS selector with data-tut attribute
            notes_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '[data-tut="notes"]'))
            )

            # Click the 'Notes' button
            self.driver.execute_script("arguments[0].click();", notes_button)

            notes = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "note_slug")]'))
            )
            test_note = notes[0]
            
            options_button = WebDriverWait(test_note, 10).until(
                EC.element_to_be_clickable((By.XPATH, './div[2]/div/button'))
            )

            self.driver.execute_script("arguments[0].click();", options_button)

            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            options = menu.find_elements(By.TAG_NAME, 'li')
            
            edit_options = options[1]

            self.driver.execute_script("arguments[0].click();", edit_options)
            
            edit_notes_input = WebDriverWait(test_note, 10).until(
                    EC.element_to_be_clickable((By.XPATH, './div[1]/div/form/div/input')))  
                
            self.driver.execute_script("arguments[0].click();", edit_notes_input)

            edit_notes_input.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            edit_notes_input.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "Test Notes" + suffix
            # Select all the existing text and delete it
            edit_notes_input.send_keys(text_to_enter)
            
            edit_notes_button = WebDriverWait(test_note, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root')))  
            
            self.driver.execute_script("arguments[0].click();", edit_notes_button)

            confirmation_popup = WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(confirmation_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            test_note_text = WebDriverWait(test_note, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.content.note-text'))
            ).text


            # Assert the expected text
            expected_text = text_to_enter
            assert test_note_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{test_note_text}'"

            print("\nSuccessfully tested the Edit Notes Feature")

            #----------Testing Share Notes Feature----------

            # Find the 'Notes' button and click it
            notes_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '[data-tut="notes"]'))
            )
            self.driver.execute_script("arguments[0].click();", notes_button)

            # Find all the note elements and select the first one
            notes = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "note_slug")]'))
            )
            test_note = notes[0]

            # Find and click the options button for the selected note
            options_button = WebDriverWait(test_note, 10).until(
                EC.element_to_be_clickable((By.XPATH, './div[2]/div/button'))
            )
            self.driver.execute_script("arguments[0].click();", options_button)

            # Open the options menu and select the 'Share' option
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )
            options = menu.find_elements(By.TAG_NAME, 'li')
            share_option = options[0]
            self.driver.execute_script("arguments[0].click();", share_option)

            # Wait for the share dialog to be displayed
            share_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Assert that the share dialog is displayed
            self.assertTrue(share_dialog.is_displayed(), "Share dialog is not displayed")
            assert share_dialog.is_displayed()

            #Close share dialog
            close_dialog = WebDriverWait(share_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
            self.driver.execute_script("arguments[0].click();", close_dialog)

            print("\nSuccessfully tested the Share Notes Feature")

            #----------Testing Delete Notes Feature----------

            # Find the 'Notes' button and click it          
            notes_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '[data-tut="notes"]'))
            )
            self.driver.execute_script("arguments[0].click();", notes_button)

            # Find all the note elements and select the first one
            notes = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "note_slug")]'))
            )
            test_note = notes[0]

            # Find and click the options button for the selected note
            options_button = WebDriverWait(test_note, 10).until(
                EC.element_to_be_clickable((By.XPATH, './div[2]/div/button'))
            )
            self.driver.execute_script("arguments[0].click();", options_button)

            # Open the options menu and select the 'Share' option
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )
            options = menu.find_elements(By.TAG_NAME, 'li')
            delete_option = options[2]
            self.driver.execute_script("arguments[0].click();", delete_option)

            confirmation_popup = WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.CLASS_NAME, 'swal-modal'))
                )

            # Find and click the 'Ok button' in warming popup
            ok_button = WebDriverWait(confirmation_popup, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm'))
            )
            
            self.driver.execute_script("arguments[0].click();", ok_button)

            # Wait for a short period to allow the popup to close and the page to update
            time.sleep(5)  # Adjust the sleep duration as needed

            print("\nSuccessfully tested the Delete Notes Feature")

            # Print success message
            print("Successfully tested the Notes - Add, Edit, Share, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Notes - Add, Edit, Share, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


    def test_reading_material(self):
        print("\nStarted testing the Reading Material - Add, Edit, Share, Delete")

        try:

            #----------Testing the Add Reading Material-Browse File----------

            #Find the video tabs
            video_tabs = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'video_tab '))
            )

            # Find the 'Reading Material' button
            reading_material_button = video_tabs[2]

            # Add a small delay before clicking the element
            time.sleep(1)  # Adjust the delay time as needed

            # Click the 'Reading Material' button
            self.driver.execute_script("arguments[0].click();", reading_material_button)

            # Find and Click 'Add Reading Material' button
            add_reading_material_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[1]')))  
            
            self.driver.execute_script("arguments[0].click();", add_reading_material_button)

            reading_material_input = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]')))  

            #Enter the input into 'title field'
            title_input_field = reading_material_input.find_element(By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/div[1]/input')
            text_to_enter = 'PDF Reading Material'
            title_input_field.send_keys(text_to_enter)

            #Enter the input into 'description field'
            description_input_field = reading_material_input.find_element(By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/div[2]/input')
            text_to_enter = 'PDF Reading Material'
            description_input_field.send_keys(text_to_enter)

            select_option = WebDriverWait(reading_material_input, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/label/div/div')))
            self.driver.execute_script("arguments[0].click();", select_option)

            #Find the 'Browse File' option and click it
            browse_file_option = WebDriverWait(reading_material_input, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/div[3]/div/ul/li[1]')))
            self.driver.execute_script("arguments[0].click();", browse_file_option)

            #Upload the file
            file_upload_input = reading_material_input.find_element(By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/label/div')

            self.driver.execute_script("arguments[0].click();", file_upload_input)

            # Wait for the file selection dialog to appear
            pyautogui.sleep(1)

            # Set the file path to be uploaded
            file_path = path = testing_pdf

            # Type the file path in the file selection dialog
            pyautogui.write(file_path)

            # Press Enter to confirm the file selection
            pyautogui.press("enter")

            # Find the submit button
            submit_button = WebDriverWait(reading_material_input, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-2"]/div[1]/div[2]/label/div/button'))
            )

            # Click the submit button
            self.driver.execute_script("arguments[0].click();", submit_button)
            
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, 'swal-text'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "File successfully added"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"

            #Close success dialog
            close_dialog = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.swal-button.swal-button--confirm')))
            self.driver.execute_script("arguments[0].click();", close_dialog)

            #----------Testing the Edit Reading Material feature----------

            reading_materials = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'container'))
            )

            test_reading_material = reading_materials[0]
            
            # Wait for the element to be present (using explicit wait)
            options_button = WebDriverWait(test_reading_material, 20).until(
                EC.element_to_be_clickable((By.XPATH, './div/button'))
            )

            self.driver.execute_script("arguments[0].click();", options_button)
            

            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            options = menu.find_elements(By.TAG_NAME, 'li')
            
            edit_options = options[2]

            self.driver.execute_script("arguments[0].click();", edit_options)
            
            
            reading_material_title = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[2]/input')))  
            
            reading_material_title.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            reading_material_title.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "PDF Reading Material" + suffix
            # Select all the existing text and delete it
            reading_material_title.send_keys(text_to_enter)


            reading_material_description = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="common-dialog-description"]/div/div[4]/textarea')))  
            
            reading_material_description.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            reading_material_description.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "PDF Reading Material" + suffix
            # Select all the existing text and delete it
            reading_material_description.send_keys(text_to_enter)
            
            submit_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))  
            
            self.driver.execute_script("arguments[0].click();", submit_button)

           # Assert the success message
            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            expected_text = "Reading Material Edited Successfully"
            actual_text = success_message.text
            assert expected_text == actual_text, f"Expected: {expected_text}, Actual: {actual_text}"

            #----------Testing the Share Reading Material feature----------

            # Find all the reading materials
            reading_materials = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'container'))
            )

            # Select the first reading material (you may need to adjust the index based on the specific reading material you want to test)
            test_reading_material = reading_materials[0]

            # Wait for the options button to be present and clickable (using explicit wait)
            options_button = WebDriverWait(test_reading_material, 20).until(
                EC.element_to_be_clickable((By.XPATH, './div/button'))
            )

            # Click the options button for the reading material
            self.driver.execute_script("arguments[0].click();", options_button)

            # Find the menu that appears after clicking the options button
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            # Find the 'share' option in the menu
            options = menu.find_elements(By.TAG_NAME, 'li')

            share_option = options[3]
            self.driver.execute_script("arguments[0].click();", share_option)

            # Wait for the share dialog to be displayed
            share_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Assert that the share dialog is displayed
            self.assertTrue(share_dialog.is_displayed(), "Share dialog is not displayed")
            assert share_dialog.is_displayed()

            #Close share dialog
            close_dialog = WebDriverWait(share_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
            self.driver.execute_script("arguments[0].click();", close_dialog)

            #----------Testing the Delete Reading Material feature----------

            # Find all the reading materials
            reading_materials = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'container'))
            )

            # Select the first reading material (you may need to adjust the index based on the specific reading material you want to test)
            test_reading_material = reading_materials[0]

            # Wait for the options button to be present and clickable (using explicit wait)
            options_button = WebDriverWait(test_reading_material, 20).until(
                EC.element_to_be_clickable((By.XPATH, './div/button'))
            )

            # Click the options button for the reading material
            self.driver.execute_script("arguments[0].click();", options_button)

            # Find the menu that appears after clicking the options button
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            # Find the 'Delete' option in the menu
            options = menu.find_elements(By.TAG_NAME, 'li')
            delete_option = options[1]

            # Click the 'Delete' option
            self.driver.execute_script("arguments[0].click();", delete_option)

            # Wait for some time (you may need to adjust the sleep time based on the behavior of the application)
            time.sleep(5)

            print("Successfully tested the Reading Material - Add, Edit, Share, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Reading Material - Add, Edit, Share, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    
    def test_qna_feature(self):
        print("\nStarted testing the Question - Add, Edit, Share, Delete and Answer - Add, Share, Delete")

        try:
            #----------Testing the Add Question feature----------

            #Find the video tabs
            video_tabs = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'video_tab '))
            )

            # Find the 'Q&A' button
            qna_button = video_tabs[3]

            # Add a small delay before clicking the element
            time.sleep(1)  # Adjust the delay time as needed

            # Click the 'q&a' button
            self.driver.execute_script("arguments[0].click();", qna_button)

            # Find and Click 'Ask Something' button
            ask_something_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-3"]/div/div[1]')))  
            
            self.driver.execute_script("arguments[0].click();", ask_something_button)

            add_question_input = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div')))  
            
            #Enter the input into 'question field'
            question_input_field = WebDriverWait(add_question_input, 10).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div')))
            text_to_enter = 'Sample Question'
            question_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(add_question_input, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))

            self.driver.execute_script("arguments[0].click();", submit_button)

            notification =  WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notificationBox"]'))
            )

            # Get the text of the notification
            notification_text = notification.text

            # Assert the expected text
            expected_text = "You got 10 bonus keys for asking a question!"
            assert  expected_text in notification_text, f"Expected text: '{expected_text}', Actual text: '{notification_text}'"

            print("\nSuccessfully tested the Add Question Feature")

            #----------Testing the Add Answer feature----------

            time.sleep(5)

            # Find all the 'Questions'
            questions = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "qna_slug")]'))
            )

            # Select the first question
            test_question = questions[0]

            
            add_answer_button = WebDriverWait(test_question, 20).until(
                EC.element_to_be_clickable((By.XPATH, './div[2]/div[2]/button[2]'))
            )

            # Click the 'Add Answer' button
            self.driver.execute_script("arguments[0].click();", add_answer_button)

            add_answer_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div')))  

            #Enter the input into 'answer field'
            answer_input_field = add_answer_dialog.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div')
            text_to_enter = 'Sample Answer'
            answer_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(add_answer_dialog, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))

            self.driver.execute_script("arguments[0].click();", submit_button)

            notification =  WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notificationBox"]'))
            )

            # Get the text of the notification
            notification_text = notification.text

            # Assert the expected text
            expected_text = "You got 10 bonus keys for answering a question!"
            assert  expected_text in notification_text, f"Expected text: '{expected_text}', Actual text: '{notification_text}'"

            print("\nSuccessfully tested the Add Answer Feature")

            #----------Testing the Share Answer feature----------

            # Find all the 'Questions'
            questions = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "qna_slug")]'))
            )

            # Select the first question
            test_question = questions[0]

            
            view_answer_button = WebDriverWait(test_question, 20).until(
                EC.element_to_be_clickable((By.XPATH, './div[2]/div[2]/button[1]'))
            )

            # Click the 'View Answer' button
            self.driver.execute_script("arguments[0].click();", view_answer_button)

            # Find all the 'Answers' to a question
            answers = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'Ans-comment-container'))
            )

            # Select the first answer
            test_answer = answers[0]

            # Wait for the options button to be present and clickable (using explicit wait)
            options_button = WebDriverWait(test_answer, 20).until(
                EC.element_to_be_clickable((By.XPATH, './button'))
            )

            # Click the options button for the reading material
            self.driver.execute_script("arguments[0].click();", options_button)

            # Find the menu that appears after clicking the options button
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            # Find the 'share' option in the menu
            options = menu.find_elements(By.TAG_NAME, 'li')

            share_option = options[2]
            self.driver.execute_script("arguments[0].click();", share_option)

            # Wait for the share dialog to be displayed
            share_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Assert that the share dialog is displayed
            self.assertTrue(share_dialog.is_displayed(), "Share dialog is not displayed")
            assert share_dialog.is_displayed()

            #Close share dialog
            close_dialog = WebDriverWait(share_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
            self.driver.execute_script("arguments[0].click();", close_dialog)

            print("\nSuccessfully tested the Share Answer Feature")

            #----------Testing Delete Answer Feature----------

            # Find all the 'Answers' to a question
            answers = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'Ans-comment-container'))
            )

            # Select the first answer
            test_answer = answers[0]

            # Wait for the options button to be present and clickable (using explicit wait)
            options_button = WebDriverWait(test_answer, 20).until(
                EC.element_to_be_clickable((By.XPATH, './button'))
            )

            # Click the options button for the reading material
            self.driver.execute_script("arguments[0].click();", options_button)

            # Find the menu that appears after clicking the options button
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            # Find the 'delete' option in the menu
            options = menu.find_elements(By.TAG_NAME, 'li')

            delete_option = options[2]
            self.driver.execute_script("arguments[0].click();", delete_option)

            time.sleep(5)

            print("\nSuccessfully tested the Delete Answer Feature")

            #----------Testing Edit Question feature----------

            back_to_questions = WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="full-width-tabpanel-3"]/div[2]/p')))

            self.driver.execute_script("arguments[0].click();", back_to_questions)

            #Find all the 'Questions'
            questions = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "qna_slug")]'))
            )

            # Select the first question
            test_question = questions[0]

            #Wait for the element to be present (using explicit wait)
            options_button = WebDriverWait(test_question, 20).until(
                EC.element_to_be_clickable((By.XPATH, './button'))
            )

            self.driver.execute_script("arguments[0].click();", options_button)
            
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            options = menu.find_elements(By.TAG_NAME, 'li')
            
            edit_options = options[1]

            self.driver.execute_script("arguments[0].click();", edit_options)

            add_question_input = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[2]/div[3]/div')))  

            #Enter the input into 'question field'
            question_input_field = add_question_input.find_element(By.XPATH, '//*[@id="common-dialog-description"]/div[2]/div[2]/div')
            
            question_input_field.send_keys(Keys.CONTROL + 'a')

            # Simulate pressing the backspace key to clear the value
            question_input_field.send_keys(Keys.BACKSPACE)

            # Generate a random suffix
            suffix = ''.join(random.choices(string.digits, k=1))

            text_to_enter = "Sample Question" + suffix
            # Select all the existing text and delete it
            question_input_field.send_keys(text_to_enter)

            # Locate and click the submit button
            submit_button = WebDriverWait(add_question_input, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedPrimary')))

            self.driver.execute_script("arguments[0].click();", submit_button)

            # Wait for the success message dialog to be visible
            success_message = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="notistack-snackbar"]'))
            )

            # Get the text of the success message
            success_message_text = success_message.text

            # Assert the expected text
            expected_text = "Question Edited"
            assert success_message_text == expected_text, f"Expected text: '{expected_text}', Actual text: '{success_message_text}'"

            print("\nSuccessfully tested the Edit Question Feature")

            #----------Testing Share Question feature----------

            #Find all the Questions
            questions = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "qna_slug")]'))
            )

            # Select the first question
            test_question = questions[0]

            #Wait for the element to be present (using explicit wait)
            options_button = WebDriverWait(test_question, 20).until(
                EC.element_to_be_clickable((By.XPATH, './button'))
            )

            self.driver.execute_script("arguments[0].click();", options_button)
            
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            options = menu.find_elements(By.TAG_NAME, 'li')

            share_option = options[4]
            self.driver.execute_script("arguments[0].click();", share_option)

            # Wait for the share dialog to be displayed
            share_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Assert that the share dialog is displayed
            self.assertTrue(share_dialog.is_displayed(), "Share dialog is not displayed")
            assert share_dialog.is_displayed()

            #Close share dialog
            close_dialog = WebDriverWait(share_dialog, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.MuiButtonBase-root.MuiIconButton-root'))
            )
            self.driver.execute_script("arguments[0].click();", close_dialog)

            print("\nSuccessfully tested the Share Question Feature")

            #----------Testing the Delete Question feature----------

            #Find all the Questions
            questions = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[contains(@id, "qna_slug")]'))
            )

            # Select the first question
            test_question = questions[0]

            #Wait for the element to be present (using explicit wait)
            options_button = WebDriverWait(test_question, 20).until(
                EC.element_to_be_clickable((By.XPATH, './button'))
            )

            self.driver.execute_script("arguments[0].click();", options_button)
            
            menu = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiList-root.MuiList-padding'))
            )

            options = menu.find_elements(By.TAG_NAME, 'li')

            delete_option = options[0]

            # Click the 'Delete' option
            self.driver.execute_script("arguments[0].click();", delete_option)

            # Wait for some time (you may need to adjust the sleep time based on the behavior of the application)
            time.sleep(5)

            print("\nSuccessfully tested the Delete Question Feature")


            print("\nSuccessfully tested Question - Add, Edit, Share, Delete and Answer - Add, Share, Delete")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Question - Add, Edit, Share, Delete and Answer - Add, Share, Delete test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)

    def test_share_tab(self):
        print("\nStarted testing the Share video tab")

        try:
            #Find the video tabs
            video_tabs = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, 'video_tab '))
            )

            # Find the 'Share' button
            share_button = video_tabs[4]

            # Add a small delay before clicking the element
            time.sleep(1)  # Adjust the delay time as needed

            # Click the 'Shaare' button
            self.driver.execute_script("arguments[0].click();", share_button)

            # Wait for the share dialog to be displayed
            share_dialog = WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, '.MuiDialog-paper'))
            )

            # Assert that the share dialog is displayed
            self.assertTrue(share_dialog.is_displayed(), "Share dialog is not displayed")
            assert share_dialog.is_displayed()

            print("Successfully tested the Share Video feature")

        except Exception as e:
            # Handle exceptions that may occur during the test
            error_message = "Share video feature test failed."
            logging.error("An error occurred: %s", error_message)
            self.fail(error_message)


if __name__ == '__main__':
    
    unittest.main()
        