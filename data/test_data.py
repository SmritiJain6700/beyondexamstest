import json
import sys


with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

baseurl = settings["baseurl"]
referral_code = settings["referral_code"]

paid_course_url = baseurl+"course/testcourse-ed9e4"+referral_code
video_page_url = baseurl+"video/ec8vSKJuZTk"+referral_code+"&course=testcourse-ed9e4&module=41033&start=0&end=17491"
new_module_test_url = "https://react-beyondexam-shone.netlify.app/course/testcourse-ed9e4"+referral_code+"&module_id=41067"
existing_module_url = "https://react-beyondexam-shone.netlify.app/course/testcourse-ed9e4"+referral_code+"&module_id=41033"
unpaid_course_url = "https://react-beyondexam-shone.netlify.app/course/create-portfolio-website-using-wordpress-e61ec"+referral_code
unerolled_course_url = "https://react-beyondexam-shone.netlify.app/course/react-testing-tutorial-d4e0a"+referral_code
valid_coupon_code = "NEW-2-34"
invalid_coupon_code = "lfp-test-101"
test_file_path = settings['beyond_exams_path'] + "\\test_files\\test_image.png"
testing_pdf = settings['beyond_exams_path'] + "\\test_files\\testing.pdf"
test_youtube_url = "https://www.youtube.com/watch?v=Gp2gu7ZFsuM"
test_course_url = "https://react-beyondexam-shone.netlify.app/course/test-course-new-development-158b2"+referral_code