import json
import sys


with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

baseurl = settings["baseurl"]
referral_code = settings["referral_code"]
home_page_url = baseurl + referral_code
login_url = baseurl + "login"
learn_a_skill_url = baseurl + "learn-a-skill" + referral_code
revise_your_syllabus_url = baseurl + "revise-your-syllabus" + referral_code
student_profile_url = baseurl + referral_code
