from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from data.urls import login_url
import time

class LoginPage:
    def __init__(self, driver):
        self.driver = driver
        self.navigate_to()

    # Define the web elements on the page
    email_input = (By.XPATH, '//*[@id="identifierId"]')
    email_next_button = (By.XPATH, '//*[@id="identifierNext"]')
    password_input = (By.XPATH, '//*[@id="password"]/div[1]/div/div[1]/input')
    login_button = (By.ID, 'passwordNext')

    def navigate_to(self):
        self.driver.get(login_url)

    # Define the methods to interact with the web elements
    def click_signup_button(self):
        signup_btn = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[5]/div/div/div[2]/div/div[2]')
        actions = ActionChains(self.driver)
        actions.move_to_element(signup_btn).click().perform()
    
    def enter_email(self, email):
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located(self.email_input))
        self.driver.find_element(*self.email_input).send_keys(email)

    def click_next_button(self):
        WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable(self.email_next_button))
        next_btn = self.driver.find_element(*self.email_next_button)
        next_btn.click()

    def enter_password(self, password):
        password_input = WebDriverWait(self.driver, 50).until(EC.element_to_be_clickable(self.password_input))
        password_input.send_keys(password)
        
    
    def click_login_button(self):
        login_btn = self.driver.find_element(*self.login_button)
        login_btn.click()
    
    def select_student_option(self):
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.CSS_SELECTOR, '.SelectorLoginBTN.btn-selector-p-2')))
        login_option = self.driver.find_element(By.CSS_SELECTOR, '.SelectorLoginBTN.btn-selector-p-2')
        if login_option.text == "Student":
            login_option.click()
            
    def select_teacher_option(self):
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="root"]/div[6]/div/div/div[2]/div/div[3]')))
        login_option = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[6]/div/div/div[2]/div/div[3]')
        if login_option.text == "Teacher":
            login_option.click()
