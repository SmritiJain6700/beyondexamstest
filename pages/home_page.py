import json
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

 
class HomePage:
    def __init__(self, driver):
        self.driver = driver
        self.load()
        
    def load(self):
        self.driver.get(settings['baseurl'])

