import json
import sys
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from data.urls import learn_a_skill_url
from data.test_data import paid_course_url, unpaid_course_url

with open('settings.json', 'r') as f:
    settings = json.load(f)
sys.path.insert(0,settings['beyond_exams_path'])

 
class CoursePage:
    def __init__(self, driver):
        self.driver = driver
        self.load()
        
    def load(self):
        self.driver.get(learn_a_skill_url)
    

    def navigate_to_a_unpaid_course(self):
        self.driver.get(unpaid_course_url)

    def navigate_to_a_paid_course(self):
        self.driver.get(paid_course_url)        

    
    def get_user_role(self):
        # Find the profile button element
        profile_button = self.driver.find_element(By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div/div/div')

        # Get the role of the user
        role_of_user = profile_button.text

        return role_of_user
    
    def switch_role(self):

        # Find the profile button element
        profile_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[1]/div/div')))

        # Perform a mouseover action using JavaScript to trigger the event
        script = "var event = new MouseEvent('mouseover', { 'view': window, 'bubbles': true, 'cancelable': true });" \
                "arguments[0].dispatchEvent(event);"
        self.driver.execute_script(script, profile_button)

        switch_role_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[1]/div[5]/div[2]/div[2]/div[5]')))

        # Click the "Switch Role" button
        self.driver.execute_script("arguments[0].click();", switch_role_button)

        switch_role_confirmation = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[4]/div[2]/button')))
        
        self.driver.execute_script("arguments[0].click();", switch_role_confirmation)

        time.sleep(5)
        

    def select_module_content_by_index(self,item_index):
        # Find the list element
        list_element = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, 'MuiList-root'))
        )

        # Find the item at the specified index
        item_xpath = f"./li[{item_index}]"
        item = list_element.find_element(By.XPATH, item_xpath)

        # Click on the item
        self.driver.execute_script("arguments[0].click();", item)